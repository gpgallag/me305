var taskControl07_8py =
[
    [ "TaskControl07", "classtaskControl07_1_1TaskControl07.html", "classtaskControl07_1_1TaskControl07" ],
    [ "Apin1", "taskControl07_8py.html#a3240e47276f67951cf6e22a627a328a6", null ],
    [ "Apin2", "taskControl07_8py.html#adbc54f0f5976c3084b4c4ec20c9ff3fa", null ],
    [ "Bpin1", "taskControl07_8py.html#abd89bd19e68cda80f15f181771200a01", null ],
    [ "Bpin2", "taskControl07_8py.html#a82c2d03b6162ca1d69e7491d940d313b", null ],
    [ "collect", "taskControl07_8py.html#aaf3d107f109b61f437e32a0e4ea876a4", null ],
    [ "enc1", "taskControl07_8py.html#ad24d30412228b57c4f20a6a31c0bfcfb", null ],
    [ "enc2", "taskControl07_8py.html#a2dea20d1e32236a4db59fbd60585f3a1", null ],
    [ "interval", "taskControl07_8py.html#a8c4652ffbf7b8401a402dc6196a5f48e", null ],
    [ "moe1", "taskControl07_8py.html#a02165ed5352b0bb78a86cf31d47f6e99", null ],
    [ "moe2", "taskControl07_8py.html#af360dd12353e024f4b68af1cc63d25e2", null ],
    [ "pin_IN1", "taskControl07_8py.html#a2c0c54b6eda33757e4c12c52971f51d1", null ],
    [ "pin_IN2", "taskControl07_8py.html#af1d6a1fdad400f9c0e98c1b9a78f04dd", null ],
    [ "pin_IN3", "taskControl07_8py.html#a7208b468ac9f029e32b3a15fd5a44449", null ],
    [ "pin_IN4", "taskControl07_8py.html#ade6badfdd03e2c3d2b0c7b650b3eafa2", null ],
    [ "pin_nSleep", "taskControl07_8py.html#a23c8349958b45a0c20560e266e080cc2", null ],
    [ "task", "taskControl07_8py.html#a0e07b7d4c54fa269c9d12d3c50d5fcaf", null ],
    [ "tim3", "taskControl07_8py.html#ab7152f8e77b6614b06724031e7bd21ac", null ],
    [ "tim4", "taskControl07_8py.html#af80c8dd1535ddaefb4b5794920a08087", null ],
    [ "tim8", "taskControl07_8py.html#a5f2eda9a224c128081ca23316bad2052", null ]
];