var searchData=
[
  ['set_5fduty_254',['set_duty',['../classmotorDriver_1_1MotorDriver.html#a4ebd4e807c868a337dd028b7c9c2ed07',1,'motorDriver::MotorDriver']]],
  ['set_5finterval_255',['set_interval',['../classclosedLoop_1_1ClosedLoop.html#a9ac131619ba1683c1eea1d8e0e80116f',1,'closedLoop::ClosedLoop']]],
  ['set_5fkd_256',['set_Kd',['../classclosedLoop_1_1ClosedLoop.html#a570d8af12476a0e8d3522c3cb52444d5',1,'closedLoop::ClosedLoop']]],
  ['set_5fki_257',['set_Ki',['../classclosedLoop_1_1ClosedLoop.html#ae22e5a424105483eaff20f6d2de29846',1,'closedLoop::ClosedLoop']]],
  ['set_5fkp_258',['set_Kp',['../classclosedLoop_1_1ClosedLoop.html#a46533e5771925356eb4f35c99f0762cb',1,'closedLoop::ClosedLoop']]],
  ['set_5fposition_259',['set_position',['../classencoderDriver_1_1EncoderDriver.html#a5f499ade88a39157a29d7e20959cf1c3',1,'encoderDriver.EncoderDriver.set_position()'],['../classmain_1_1DataCollect.html#a396314dfe855389f8952d7930dc0da6f',1,'main.DataCollect.set_position()']]],
  ['setfrequency_260',['setFrequency',['../classLEDDriver_1_1LEDDriver.html#a9fe79785152264dadbac19a2f67c3f7d',1,'LEDDriver::LEDDriver']]],
  ['stop_261',['stop',['../classelevator_1_1MotorDriver.html#a9bfb6b047b3e3ca985d9d282524b54ab',1,'elevator::MotorDriver']]]
];
