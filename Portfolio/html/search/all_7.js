var searchData=
[
  ['gear_5fratio_53',['gear_ratio',['../classtaskControl_1_1TaskControl.html#a33065276cf83e9f2f067ba571df02bc4',1,'taskControl.TaskControl.gear_ratio()'],['../classtaskControl07_1_1TaskControl07.html#a9f2865d0356df91b18dac6522de3ae3b',1,'taskControl07.TaskControl07.gear_ratio()']]],
  ['get_5fdelta_54',['get_delta',['../classencoderDriver_1_1EncoderDriver.html#a95f13d24470c3ee417cbfd6f8b766c19',1,'encoderDriver.EncoderDriver.get_delta()'],['../classmain_1_1DataCollect.html#a0b2f96f8a1b94d31459c826e46880638',1,'main.DataCollect.get_delta()']]],
  ['get_5fkd_55',['get_Kd',['../classclosedLoop_1_1ClosedLoop.html#aaf2b8155ce84affc8f84f5d075da094c',1,'closedLoop::ClosedLoop']]],
  ['get_5fki_56',['get_Ki',['../classclosedLoop_1_1ClosedLoop.html#a91d15fd32e50b07ce48f0d9e333c9c1e',1,'closedLoop::ClosedLoop']]],
  ['get_5fkp_57',['get_Kp',['../classclosedLoop_1_1ClosedLoop.html#a2671d5733ffebe5e21caf380028d6880',1,'closedLoop::ClosedLoop']]],
  ['get_5fperformance_58',['get_performance',['../classinterfaceFront07_1_1InterfaceFront07.html#a5a09a98ffffc98c2edd8385bf0c928d6',1,'interfaceFront07::InterfaceFront07']]],
  ['getbuttonstate_59',['getButtonState',['../classelevator_1_1Button.html#ab4604119159272416784795296d27e6f',1,'elevator::Button']]],
  ['getmotorstate_60',['getMotorState',['../classelevator_1_1MotorDriver.html#a6b2e861e143d7ff911d93e65ad4716a4',1,'elevator::MotorDriver']]]
];
