var shares_8py =
[
    [ "array_position_meas", "shares_8py.html#a77df72d7216134532cd9fe3be5f329ae", null ],
    [ "array_position_ref", "shares_8py.html#a7c188a4c8439896263e6b65eec1bdbd1", null ],
    [ "array_time_meas", "shares_8py.html#a40aa08809a991fdb786e3e79af477d26", null ],
    [ "array_time_ref", "shares_8py.html#a4b113e421a8ecf9a9a3e11ec270f7536", null ],
    [ "array_W_meas", "shares_8py.html#a837e9c260dbbfcde1333d44b0c6e9137", null ],
    [ "array_W_ref", "shares_8py.html#a953b31016e1fcee5f895689b90d2419f", null ],
    [ "cmd", "shares_8py.html#a0b182f100c714a2a5a21a35103586edd", null ],
    [ "collect", "shares_8py.html#a7da3040515b03b27f3f8149ce4195648", null ],
    [ "Kd", "shares_8py.html#a5ec23a1804f4f17ba7a47b990eb189ed", null ],
    [ "Ki", "shares_8py.html#a51639b9b0ede471b9356b14746273d6d", null ],
    [ "Kp", "shares_8py.html#a8478a8308313d091682c7fbb0b5f3153", null ],
    [ "resp", "shares_8py.html#a48ac52398fd208b94afab27e4c5440c2", null ],
    [ "W_ref", "shares_8py.html#aa29f7c46e499b7382f6e1a8444cda681", null ]
];