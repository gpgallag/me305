var classinterfaceRemote_1_1InterfaceRemote =
[
    [ "__init__", "classinterfaceRemote_1_1InterfaceRemote.html#a2dcfe1208bdddce49ca825cecfaa227e", null ],
    [ "run", "classinterfaceRemote_1_1InterfaceRemote.html#ab7ce92f1784d7134886adecb62b1546a", null ],
    [ "transitionTo", "classinterfaceRemote_1_1InterfaceRemote.html#ad5bd3f28fa7c6c68c3bfd8beae7c6b73", null ],
    [ "collecting_time", "classinterfaceRemote_1_1InterfaceRemote.html#a105962a80feaa923387dec6b57a111ed", null ],
    [ "interval", "classinterfaceRemote_1_1InterfaceRemote.html#a4c0cd0a217b20be6b3d6aef98e6ef16d", null ],
    [ "next_time", "classinterfaceRemote_1_1InterfaceRemote.html#a323c7f6e1d1b808002ffa86802217e93", null ],
    [ "start_time", "classinterfaceRemote_1_1InterfaceRemote.html#a84544f7e7b55ac0a4f55c04ea5385c66", null ],
    [ "state", "classinterfaceRemote_1_1InterfaceRemote.html#a901619a7e7c29a6dd39f95e3a0e25e34", null ],
    [ "uart", "classinterfaceRemote_1_1InterfaceRemote.html#afbbd126924772751ae405814aa8b3ba4", null ],
    [ "user_input", "classinterfaceRemote_1_1InterfaceRemote.html#a50f8c1a41657c93bd91b251a05d508b6", null ]
];