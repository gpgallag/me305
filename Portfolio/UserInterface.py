'''
@file UserInterface.py

@brief This file contains a finite-state machine to run a UserInterface object.

@details The UserInterface task provides the user to interface with a remote program
on the Nucleo via the Spyder terminal. More specifically, this task interacts with the
DataCollect task object that must be uploaded to the Nucleo as 'main.py'. The task
interacts with the DataCollect task via UART.

@package Lab0x04

@brief This package contains UserInterface.py and main.py.

@author Grant Gallagher

@date November 3, 2020
'''

import time
import serial
import array
import matplotlib.pyplot as plt
import csv

class UserInterface:
    '''
    @brief   This class encapsulates the front-end of a user interface measure encoder position.
    
    @details This class contains a finite-state machine that is used to interact
             with an encoder driver through a back-end controller. The front-end UI
             is capable of sending commands start the encoder collection recording,
             stop the recording, and export a plot/.csv of the results.
    '''
    ## State 0: Initialization
    S0_INIT          = 0
    
    ## State 1: Wait for user input.
    S1_WAIT          = 1
    
    ## State 2: Waiting for response from data collection task.
    S2_WAIT_FOR_RESP = 2

    def __init__(self, interval):
        '''
        @brief Constructs a UserInterface object.
        @details Initializes starting and current time of the task, sets up the
                 the serial port for communication with the nucleo, and initializes
                 the time/position arrays that are collected from the Nucleo. By default,
                 the constructor initalizes the serial port to COM3 with a baudrate of 115200.
        @param interval An integer number of seconds between desired runs of the task.
        @code Example:
            
            from userTask import UserTask # Import the module
            
            task_1 = UserInterface(0.1) # Setting up the task with an interval of 100ms
        '''
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
                
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval

        ## The Serial Port object.
        self.serial_port = serial.Serial('COM3', baudrate = 115200 ,timeout = 1)
        self.serial_port.close()
        
        ## The conversion factor for encoder ticks to degres of rotation.
        self.conversion_factor = 7*4*50/360
        
        ## An array containing encoder position since recording.
        self.array_position = array.array("f")
        
        ## An array containing time since recording.
        self.array_time = array.array("f")
        
        ## The current state of the data collection task.
        self.task_finished = False
        

    def run(self):
        '''
        @brief Runs one iteration of the task
        @details The UserInterface.run() method should be ran continuously until
                 the FSM is finished.
        @code Example:
            
            while task.task_finished == False: # Runs the task forever until it is finished.
                task.run() # Constantly calls the run() method
        '''
        ## The timestamp for the current task run.
        self.current_time = time.time()
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if self.current_time - self.next_time >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print("Weclome to the encoder user-interface using Spyder!")
                print("Interact with the encoder using the following commands:")
                print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
                print("        G - Begin recording encoder position")
                print("        S - Stop recording and export data")
                print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n")
                self.transitionTo(self.S1_WAIT) # Transition to State 1
        
            elif(self.state == self.S1_WAIT):
                # Run State 1 Code
                ## The user input from within the Spyder terminal.
                cmd = input("Command: ") # Prompts the user for an input
                if cmd != "":          # Checks for nonnull user input
                    self.serial_port.open()                          # Opens Nucleo serial
                    self.serial_port.write(str(cmd).encode('ascii')) # Sends user input to Nucleo serial
                    self.transitionTo(self.S2_WAIT_FOR_RESP) # Transition to State 2
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                ## Output from Nucleo serial.
                self.output = str(self.serial_port.readline().decode('ascii'))
                self.serial_port.close() # Close Nucleo serial
                if self.output[0] == 'p':
                    self.convert_to_array(self.output)
                    self.export_plot()
                    self.export_data()
                    self.task_finished = True
                else:
                    print(self.output) 
                self.output = ""
                self.transitionTo(self.S1_WAIT)
                    
            else:
                # Error handling
                pass
                        
            self.next_time = self.next_time + self.interval # Update the "Scheduled" timestamp 

    def transitionTo(self, newState):
        '''
        @brief Updates the current state variable.
        @param newState The state in the FSM to transition to.
        '''
        self.state = newState
        
    def convert_to_array(self, string):
        '''
        @brief Converts the position/timestamp array sent from the UART into two seperate arrays.
        '''
        ## Stores the numerical value of the UART output as an integer.
        self.string_number = 0
        ## A placeholder to determine if the UART output value is negative.
        self.is_negative = 1
        ## A reference for the position and time arrays coming from the UART.
        self.ref_array= self.array_position
        for index in range(0, len(string)-1): # Parses the string sent by the UART
            if string[index] == "-":          # Checks for a negative number
                self.is_negative = -1         # Assigns placeholder value
            elif string[index] == "t":        # Checks for the beginning of the time array
                self.ref_array= self.array_time
                self.conversion_factor = 0.001 # Changes the conversion factor from position to time (ms to s)
            elif str(string[index]).isnumeric(): # Checks for a numerical value
                self.string_number = self.string_number * 10 + int(string[index])
            if string[index] == "," or string[index] == "]":
                self.ref_array.append(self.string_number * self.is_negative * self.conversion_factor)
                self.string_number = 0 # Resets the value for next iteration
                self.is_negative = 1   # Resets the value for next iteration    
        self.array_position.remove(0)
        self.array_time.remove(0)
        
    def export_plot(self):
        '''
        @brief Creates and exports a plot for the encoder position versus time as a .png file.
        @details Labels the y-axis (position), x-axis (time), and title of the plot, and exports it as a "encoder_plot.png".
                 By default, this method exports a plot with a predetermined file name,
                 axis labels, and title - the y-axis is position (degrees), and the x-axis
                 is time (seconds). The file is exported as 'encoder_plot.png'.
        '''
        plt.plot(self.array_time, self.array_position) # Plots the encoder position and time
        plt.xlabel('Time, t [s]')                      # Label x-axis
        plt.ylabel('Position, θ [degrees]')            # Label y-axis
        plt.title('Encoder Position versus Time')      # Label plot title
        plt.savefig('encoder_plot.png')                # Save the plot under file 
        print('The encoder plot has been exported as: "encoder_plot.png"')
        
    def export_data(self):
        '''
        @brief Exports the encoder raw data as a .csv file to the current folder.
        @details The raw data is exported as a comma seperated variable file,
                 labeled as 'encoder_data.csv'. The position data is stored in
                 column A, and the time data is stored in column B.
        '''
        with open('encoder_data.csv', mode = 'w') as encoder_file: # Open a .csv file in write mode
            ## A writer object responsible for converting the user’s data into delimited strings
            self.value_writer = csv.writer(encoder_file, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_MINIMAL) # Formats the .csv file
            for data_set in range(len(self.array_position)-1):                                           # Parses the position/time arrays for data
                self.value_writer.writerow( [self.array_position[data_set], self.array_time[data_set]] ) # Writes the position/time for the nth data set
        print('The raw encoder data has been exported as: "encoder_data.csv"')
            
## A UserInterface task object with a timing interval of 0.1s
task_1 = UserInterface(0.1) # Construct a UserInterface object

while task_1.task_finished == False:
    task_1.run()
