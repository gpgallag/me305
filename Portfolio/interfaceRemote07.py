'''
@file interfaceRemote07.py

@brief This file contains a finite-state machine to run the remote end of a user interface

@details The InterfaceRemote07 task behaves as the back-end task for a UI. The remote interface
runs locally on the Nucleo device, and receives commands from the front interface which is
ran on the communicating PC. The remote interface is capable of receiving commands which:
change the Ki,Kp, Kd, and motor reference speed for the controller, and also to print
the step response data. Note: This class is not the same as InterfaceRemote
which is is found in package 0x06. This receives an array of reference times, positions,
and velocities for a motor profile via the UART and commands the controller to track the profile.
Then, the measured positions and velocities of the system profile are sent to the
front end of the UI.

@package Lab0x07

@author Grant Gallagher

@date December 4, 2020
'''
import shares
import pyb
from pyb import UART

class InterfaceRemote07: 
    '''
    @brief   This class encapsulates the back-end of a user interface used to control a motor
             reference tracking controller.
    
    @details This class contains a finite-state machine that is used to interact
             with a back-end UI (ran on remotely on a Nucleo). Subsequently, the UI can
             indirectly interact with a PID controller which is used to control the
             reference profile of a motor.

    '''
    ## State 0: Initialization
    S0_INIT = 0
    
    ## State 1: Wait for user input.
    S1_WAIT = 1
    
    ## State 3: Collect encoder data.
    S2_COLLECT_DATA = 2
       
    
    def __init__(self, interval):
        '''
        @brief Constructs a InterfaceFront task object.
        @param interval A number of milliseconds between desired runs of the task.
        '''
        ##  The amount of time in milliseconds between runs of the task.
        self.interval = interval
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration.
        self.start_time = pyb.millis()
        
        ## The "timestamp" for when the task should run next.
        self.next_time = self.interval + pyb.elapsed_millis(self.start_time)
        
        ## The UART console.
        self.uart = UART(2)
        
        ## The character value of the user input.
        self.user_input = 0
        
        ## The time of when data collection begins.
        self.collecting_time = 0

    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if (pyb.elapsed_millis(self.start_time) >= self.next_time):  
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code   
                # # print("Time: " + str(pyb.elapsed_millis(self.start_time)))
                if self.uart.any() >= 6: # Check for user input
                    self.user_input = self.uart.readline(6).decode('ascii') # Store user input
                    # # print(self.user_input)
                    if self.user_input[0] == 't':
                        self.user_input = self.user_input.replace('t','')
                        self.user_input = self.user_input.replace('x','')
                        shares.array_time_ref.append(int(self.user_input))
                    elif self.user_input[0] == 'p':
                        self.user_input = self.user_input.replace('p','')
                        self.user_input = self.user_input.replace('x','')
                        shares.array_position_ref.append(int(self.user_input))
                    elif self.user_input[0] == 'w':
                        # print((self.user_input))
                        self.user_input = self.user_input.replace('w','')
                        self.user_input = self.user_input.replace('x','')
                        # print(str(self.user_input) + '\n')
                        shares.array_W_ref.append(int(self.user_input))
                        
                # print("time: " + str(len(shares.array_time_ref)))
                # print("          pos: " + str(len(shares.array_position_ref)))
                # print("                    W: " + str(len(shares.array_W_ref)))
                if (len(shares.array_position_ref) == len(shares.array_time_ref)) and (len(shares.array_W_ref) == len(shares.array_time_ref)) and (len(shares.array_time_ref) > 0):
                    self.transitionTo(self.S1_WAIT)
                    # print("Time")
                    # print(shares.array_time_ref)
                    # print("postiion")
                    # print(shares.array_position_ref)
                    # print("W")
                    # print(shares.array_W_ref)
                
                
            elif(self.state == self.S1_WAIT):
                # Run State 1 Code
                if self.uart.any() != 0: # Check for user input
                    self.user_input = self.uart.readline().decode('ascii') # Store user input
                    if self.user_input[0:2] == 'Kp':           # User sent a Kp value
                        shares.Kp = float(self.user_input[2:])
                        self.uart.write("Kp" + str(shares.Kp))
                        
                    elif self.user_input[0:2] == 'Ki':         # User sent a Ki value
                        shares.Ki = float(self.user_input[2:]) # Store value in shares
                        self.uart.write("Ki" + str(shares.Ki)) # Respond to UI with value
                        
                    elif self.user_input[0:2] == 'Kd':         # User sent a Kd value
                        shares.Kd = float(self.user_input[2:]) # Store value in shares
                        self.uart.write("Kd" + str(shares.Kd)) # Respond to UI with value
                        
                    elif (self.user_input == 'S'): # User command to step motor
                        # print("Starting motor step.")
                        shares.collect = True      # Activate taskControl State 2
                        self.transitionTo(self.S2_COLLECT_DATA)  # Transition to State 2
                        
                    elif (self.user_input == 'E'): # User command to export the step data
                        self.uart.write("p" + str(shares.array_position_meas) + ",W" + str(shares.array_W_meas)) # Output data
                        
                    else:
                        self.uart.write('E') # The UI back-end was unable to recognize the command. Error handling.
                        pass
                        
                    
            elif(self.state == self.S2_COLLECT_DATA):
                # Run State 2 Code   
                # The InterfaceRemote07 will not do anything until TaskControl is finished  collecting data.
                if shares.collect == False: 
                    self.transitionTo(self.S1_WAIT)
                else:
                    # Error handling
                    pass
                
            else:
                # Error handling
                pass
                
            self.next_time += self.interval # Update the "Scheduled" timestamp 
                
    def transitionTo(self, newState):
        '''
        @brief Updates the current state variable.
        @details When called, transitions from the current state of the FSM
                 to the next state.
        @param newState The state in the FSM to transition to.
        '''
        self.state = newState
        