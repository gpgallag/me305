'''
@file interfaceFront07.py

@brief This file contains a finite-state machine that runs the front-end of a user interface.

@details The InterfaceFront07 task behaves as a UI that runs locally on a PC. The interface
interacts with a back-end userface, InterfaceRemote, that is stored remotely on the Nucleo
L476RG. Within this UI, the user can specify the P, I, and D gains for a motor controller,
set the reference velocity, step the motor, and export the respective data/plot. Note: This
class is not the same as InterfaceFront which is is found in package 0x06. This UI task
reads from a CSV local to the compiler, send the time/position/velocity reference values
to the nucleo, and then exports the measured/reference profile plots of the position and velocity - 
finally, calculating the performance metric, J.

@package Lab0x07

@brief This package contains interfaceFront07.py, interfaceRemote07.py, taskControl07.py,
       closedLoop.py, motorDriver.py, encoderDriver.py, and shares.py.

@author Grant Gallagher

@date December 4, 2020
'''

import time
import serial
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import csv

class InterfaceFront07:
    '''
    @brief   This class encapsulates the front-end of a user interface used to control a motor.
    
    @details This class contains a finite-state machine that is used to interact
             between the front-end of the UI (ran on a PC) and a controller task.
             The front-end UI is capable of sending commands to change/set the PID gains
             of the controller, command the controller task to drive the motor
             according to a reference profile, and export a graph/.csv file containing the
             subplots of the position and velocity of the reference/measured profile.
    '''
    ## State 0: Initialization
    S0_INIT          = 0
    
    ## State 1: Wait for user input.
    S1_WAIT_FOR_CMD  = 1
    
    ## State 2: Waiting for response from the back-end of the UI.
    S2_WAIT_FOR_RESP = 2

    def __init__(self, interval):
        '''
        @brief Constructs a InterfaceFront07 task object.
        @param interval A number of seconds between desired runs of the task.
        '''
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
                
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval

        ## The Serial Port object.
        self.serial_port = serial.Serial('COM5', baudrate = 115200 ,timeout = 1)
        self.serial_port.close()
        
        ## The command variable which stores the user's input.
        self.cmd = ''
        
        ## The current state of the data collection task.
        self.task_finished = False
        
        ## The conversion factor for sorting the .csv output.
        self.conversion_factor = 1
        
        ## The response variable which prompts the user with feedback.
        self.response = ''
        
        ## The controller proportional gain.
        self.Kp = 0.1
        
        ## The controller integral gain.
        self.Ki = 0.2
        
        ## The controller derivative gain.
        self.Kd = 0.001
        
        ## A list containing the condensed reference time profile data imported from a CSV. [ms]
        self.array_time_ref = []
        
        ## A list containing the condensed reference position profile data imported from a CSV. [degrees]
        self.array_position_ref = []
        
        ## A list containing the condensed reference velocity profile data imported from a CSV. [RPM]
        self.array_W_ref = []
        
        ## A list containing the measured position profile of a system. [degrees]
        self.array_position_meas = []
        
        ## A list containing the measured velocity profile of a system. [RPM]
        self.array_W_meas = []

        

    def run(self):
        '''
        @brief Runs one iteration of the task.
        '''
        ## The timestamp for the current task run.
        self.current_time = time.time()
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if self.current_time - self.next_time >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code              
                
                
                print("Importing 'reference.csv'...")
                ref = open('reference.csv')
                while True:
                    line = ref.readline()
                   
                    if line == '':
                        break
                   
                    else:
                        (t,v,x) = line.strip().split(',');
                    if float(t) * 1000 % 20 == 0:
                        self.array_time_ref.append(int(float(t) * 1000))
                        self.array_W_ref.append(int(float(v)))
                        self.array_position_ref.append(int(float(x)))

                ref.close()      
                sample_interval = 0.02
                print("Writing reference times to Nucleo...")
                self.serial_port.open() # Opens Nucleo serial
                for value in self.array_time_ref:
                    value = 't' + str(value)
                    if len(value) < 6:
                        for count in range(6-len(value)):
                            value += 'x'
                    self.serial_port.write(value.encode('ascii')) # Sends user input to Nucleo serial
                    time.sleep(sample_interval)                        
                time.sleep(sample_interval)
                
                print("Writing reference positions to Nucleo...")
                for value in self.array_position_ref:
                    value = 'p' + str(value)
                    if len(value) < 6:
                        for count in range(6-len(value)):
                            value += 'x'
                    self.serial_port.write(value.encode('ascii')) # Sends user input to Nucleo serial
                    time.sleep(sample_interval) 
                    
                time.sleep(sample_interval)                        
                
                print("Writing reference velocities to Nucleo...")
                for value in self.array_W_ref:
                    value = 'w' + str(value)
                    if len(value) < 6:
                        for count in range(6-len(value)):
                            value += 'x'
                    self.serial_port.write(value.encode('ascii')) # Sends user input to Nucleo serial
                    time.sleep(sample_interval)    

                time.sleep(sample_interval)                        
                self.serial_port.close()
                
                self.response = ('Initialization complete. Commands and current parameters are listed above.')
                self.transitionTo(self.S1_WAIT_FOR_CMD) # Transition to State 1
        
        
            elif(self.state == self.S1_WAIT_FOR_CMD):
                # Run State 1 Code
                self.clear()          # Clear the serial port
                self.print_commands() # Prompt the text-based UI
                time.sleep(0.2)
                print('\nResponse: '+self.response+'') # Display user feedback
                time.sleep(0.2)
                self.cmd = input("Command: ") # Prompt user input
                
                if self.cmd == '': # Check for non-empty input
                    self.response = ('Error. Command was empty.')
                    
                elif self.cmd == 'p' or self.cmd == 'P': # Checks for P gain command
                    self.cmd = input('Set the Kp value to: ')
                    try: # Checks if input is numeric
                        float(self.cmd)  
                        if float(self.cmd) >= 0: # Checks if input is positive
                            self.serial_port.open() # Opens Nucleo serial
                            self.serial_port.write(str('Kp'+self.cmd).encode('ascii')) # Sends user input to Nucleo serial
                            self.response = ('The proportional gain has been set to '+str(self.cmd))
                            self.transitionTo(self.S2_WAIT_FOR_RESP) # Transition to State 2
                        else:
                            raise ValueError
                    except ValueError:
                        self.response = ("'"+str(self.cmd)+"' is not a valid gain. Please input a positive number.")
                        
                elif self.cmd == 'i' or self.cmd == 'I': # Checks for I gain command
                    self.cmd = input('Set the Ki value to: ')
                    try: # Checks if input is numeric
                        float(self.cmd)
                        if float(self.cmd) >= 0: # Checks if input is positive
                            self.serial_port.open() # Opens Nucleo serial
                            self.serial_port.write(str('Ki'+self.cmd).encode('ascii')) # Sends user input to Nucleo serial
                            self.response = ('The integral gain has been set to '+str(self.cmd))
                            self.transitionTo(self.S2_WAIT_FOR_RESP) # Transition to State 2
                        else:
                            raise ValueError
                    except ValueError:
                        self.response = ("'"+str(self.cmd)+"' is not a valid gain. Please input a positive number.")
                        
                elif self.cmd == 'd' or self.cmd == 'D': # Checks for D gain command
                    self.cmd = input('Set the Kd value to: ')
                    try: # Checks if input is numeric
                        float(self.cmd)
                        if float(self.cmd) >= 0: # Checks if input is positive
                            self.serial_port.open() # Opens Nucleo serial
                            self.serial_port.write(str('Kd'+self.cmd).encode('ascii')) # Sends user input to Nucleo serial
                            self.response = ("The derivative gain has been set to "+str(self.cmd))
                            self.transitionTo(self.S2_WAIT_FOR_RESP) # Transition to State 2
                        else:
                            raise ValueError
                    except ValueError:
                        self.response = ("'"+str(self.cmd)+"' is not a valid gain. Please input a positive number.")
                        
                elif self.cmd == 's' or self.cmd == 'S':     # Checks for start command
                    self.serial_port.open()                  # Opens Nucleo serial
                    self.serial_port.write('S'.encode('ascii'))
                    self.response = ('The step response test has been initiated.')
                    self.serial_port.close()                  # Opens Nucleo serial
                    
                elif self.cmd == 'e' or self.cmd == 'E':     # Checks for export command
                    self.serial_port.open()                  # Opens Nucleo serial
                    self.serial_port.write('E'.encode('ascii'))
                    self.transitionTo(self.S2_WAIT_FOR_RESP) # Transition to State 2
                else:
                    print("Error. Command is not recognized")
                    pass
                
                print("Processing...")
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                ## Output from Nucleo serial.
                self.output = str(self.serial_port.readline().decode('ascii'))
                if self.output != '': # Checks for non-empty response
                    if self.output[0] == 'p': # 'p' is the beginning of the array
                        print("Got a P")
                        print("Here's the list: " +str(self.output))
                        self.convert_to_list(self.output)
                        self.serial_port.close()
                        self.clear()
                        self.export_plot()  # Export plot
                        self.export_data()  # Export .csv
                        print("The performance metric of the system is: J = " + str(self.get_performance()))
                        self.task_finished = True # Close the program
                        
                    elif self.output[0:2] == 'Kp': # Kp gain response
                        self.Kp = self.output[2:]  # Assign remaining characters
                        self.serial_port.close()   # Close Nucleo serial
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                        
                    elif self.output[0:2] == 'Ki': # Ki gain response
                        self.Ki = self.output[2:]  # Assign remaining characters
                        self.serial_port.close()   # Close Nucleo serial
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                                
                    elif self.output[0:2] == 'Kd': # Kd gain response
                        self.Kd = self.output[2:]  # Assign remaining characters
                        self.serial_port.close()   # Close Nucleo serial
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                    
                    elif self.output == 'E': # Error response
                        self.response = ('Error. The remote UI does not know how to respond')
                        self.serial_port.close()   # Close Nucleo serial
                    else:
                        pass
                    
                else:
                    pass
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                        
            self.next_time = self.next_time + self.interval # Update the "Scheduled" timestamp 

    def transitionTo(self, newState):
        '''
        @brief Updates the current state variable.
        @details When called, transitions from the current state of the FSM
                 to the next state.
        @param newState The state in the FSM to transition to.
        '''
        self.state = newState
        
    def convert_to_list(self, string):
        '''
        @brief Converts the compressed array sent from the back-end to individual arrays.
        @param string The compressed array of motor speeds and timestamps.
        '''
        print(string)
        string = string.replace('array','')
        string = string.replace('i','')
        string = string.replace(']','')
        string = string.replace('[','')
        string = string.replace(']','')
        string = string.replace('(','')
        string = string.replace(')','')
        string = string.replace(' ','')
        string = string.replace("'",'')
        print(string)
        
        ## The temporary list of measured position and velocity profile datapoints sent from the Nucleo.
        self.temp_list = string.split(',')
        print(str(self.temp_list))
        while len(self.temp_list) > 0:
            # print("withing while loop     " + str(len(self.temp_list)))
            if self.temp_list[0] == 'p':
                self.temp_list.pop(0)
                while len(self.temp_list) > 0:
                    if self.temp_list[0] == 'W':
                        break
                    else:
                        self.array_position_meas.append(int(self.temp_list[0]))
                        self.temp_list.pop(0)
                
            elif self.temp_list[0] == 'W':
                self.temp_list.pop(0)
                while len(self.temp_list) > 0:
                    self.array_W_meas.append(int(self.temp_list[0]))
                    self.temp_list.pop(0)
        
    def export_plot(self):
        '''
        @brief Creates and exports a plot for the motor speed versus time as a .png file.
        @details Labels the y-axis (speed), x-axis (time), and title of the plot, and exports it as a "motor_plot.png".
        '''
        for timestamp in range(len(self.array_time_ref) - 1):
            self.array_time_ref[timestamp] /= 1000 
        self.array_time_ref.pop()
        self.array_position_ref.pop()
        self.array_W_ref.pop()
        
        P_ref_legend = mpatches.Patch(color = 'orange', label = 'Ω_ref')
        P_meas_legend = mpatches.Patch(color = 'blue', label = 'Ω_meas')
        W_ref_legend = mpatches.Patch(color = 'orange', label = 'θ_ref')
        W_meas_legend = mpatches.Patch(color = 'blue', label = 'θ_meas')
        
        
        fig, (ax1, ax2) = plt.subplots(2, figsize = (8,5))
        plt.subplots_adjust(hspace = .75)
        ax1.plot(self.array_time_ref, self.array_W_meas)
        ax1.plot(self.array_time_ref, self.array_W_ref)
        ax1.set(xlabel = 'Time, t [s]', ylabel = 'Velocity, Ω [RPM]')
        ax1.set_title('Velocity Response')
        ax1.legend(handles = [W_ref_legend, W_meas_legend], loc = 'lower right', fontsize = 'small')
        ax2.plot(self.array_time_ref, self.array_position_meas)
        ax2.plot(self.array_time_ref, self.array_position_ref)
        ax2.set(xlabel = 'Time, t [s]', ylabel = 'Position, θ [deg]')
        ax2.set_title('Position Response')
        ax2.legend(handles = [P_ref_legend, P_meas_legend], loc = 'lower right', fontsize = 'small')
        
        fig.savefig('motor_plot.png')
        print('Data exported as motor_plot.png')
        print('The step response plot has been exported as: "motor_plot.png"')
        
    def export_data(self):
        '''
        @brief Exports the encoder raw data as a .csv file to the current folder.
        @details The raw data is exported as "motor_data.csv".
        '''
        with open('motor_data.csv', mode = 'w') as encoder_file: # Open a .csv file in write mode
            ## A writer object responsible for converting the user’s data into delimited strings
            self.value_writer = csv.writer(encoder_file, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_MINIMAL) # Formats the .csv file
            for data_set in range(len(self.array_position_meas)-1):                                           # Parses the position/time arrays for data
                self.value_writer.writerow( [self.array_time_ref[data_set], self.array_position_meas[data_set], self.array_W_meas[data_set]] ) # Writes the position/time for the nth data set
        print('The raw motor data has been exported as: "motor_data.csv"')
        
    def clear(self):
        '''
        @brief      Flushes the serial port of any chararacters.
        '''
        print("\x1B\x5B2J")
        print("\x1B\x5BH")
        
    def print_commands(self):
        '''
        @brief      Prints the list of available commands to comminicate with the UI back-end on the Nucleo.
        '''
        print("                      Console Commands:")
        print("-------------------------------------------------------------------")
        print("P - Set proportial gain           S - Start motor step response")
        print("I - Set integral gain             E - Export data and plot")
        print("D - Set derivative gain")
        print("-------------------------------------------------------------------\n")
        print('Current parameters:')
        print('    Kp = '+str(self.Kp))
        print('    Ki = '+str(self.Ki))
        print('    Kd = '+str(self.Kd))
        
    def get_performance(self):
        '''
        @brief      Calculates the performance metric of the the system.
        @return J   The perforamnce metric of the system.
        '''
        integral = 0
        
        
        for index in range(len(self.array_time_ref)-1):
            integral += (self.array_W_ref[index] - self.array_W_meas[index])**2 + (self.array_position_ref[index] - self.array_position_meas[index])**2
        
        J = 1/len(self.array_time_ref)
        
        return J
        
            
        
if __name__ == '__main__':
    ## A UserInterface task object with a timing interval of 0.1s
    UI_task = InterfaceFront07(.1) # Construct a UserInterface object

    while UI_task.task_finished == False:
        UI_task.run()
