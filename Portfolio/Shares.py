'''
@file shares.py

@brief A container for all the inter-task variables.

@package Lab0x06
@package Lab0x07
'''
import array
## The shared command character sent from Front
cmd = None

## The shared response character sent from Remote
resp = None

## The shared state of the ClosedLoop task that represents if it is active.
collect = False

## The shared proportional gain value.
Kp = .1

## The shared integral gain value.
Ki = .1

## The shared derivative gain value.
Kd = 0.02

## The reference motor speed for step response.
W_ref = 700

## A shared array containing the measured timestamps for a system profile since recording.
array_time_meas = array.array("i")

## A shared array containing the measured encoder speed profile since recording.
array_W_meas = array.array("i")

## A shared array containing the measured encoder position profile since recording.
array_position_meas = array.array("i")

## A shared array containing the reference timestamps for a system profile.
array_time_ref      = array.array("i")

## A shared array containing a reference encoder position profile.
array_position_ref = array.array("i")

## A shared array containing a reference encoder speed profile.
array_W_ref        = array.array("i")


