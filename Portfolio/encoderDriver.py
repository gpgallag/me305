'''
@file encoderDriver.py

@brief This file contains a driver for a Hall-effect encoder in quadrature.

@details The driver is initialized with the requisite timer, timer channels, and pin
locations for the Nucleo L476RG microprocessor, as inputted by the user.
The driver does *not* check whether the desired parameters are acceptable - please
refer to the L476RG data sheet for more information on hardware. The driver
also corrects for under/overflow.

@package Lab0x04
@package Lab0x06
@package Lab0x07

@author Grant Gallagher

@date December 4, 2020
'''
import pyb

class EncoderDriver():
    '''
    @brief      Encapsulates the operation of a timer to read from an encoder.
    
    @details    This class initializes the pin locations, timer, and timer
                channels (for each pin) for an encoder. The class reads the
                position of the encoder from two Hall effect sensors in quadrature
                while correcting for under/overflow. A run task is created to
                constantly update the encoder position and delta at a regular
                interval to avoid missing encoder ticks.
    '''
    def __init__(self, pin_A, pin_B, timer, debug):
        '''
        @brief       Constructs an EncoderDriver object.
        @param pin_A (1/2) of the signal pins for the hall-effect sensor on the encoder.
        @param pin_B (2/2) of the signal pins for the hall-effect sensor on the encoder.
        @param timer A timer object with prescalar and period used to construct the timer channels.
        @param debug A boolean value that determines debugging state.
        @code Example:
            
            Apin = pyb.Pin(pyb.Pin.cpu.A6)                     # Encoder Apin initialized to pin A6 on the Nucleo
            Bpin = pyb.Pin(pyb.Pin.cpu.A7)                     # Encoder Bpin initialized to pin A7 on the Nucleo
            tim = pyb.Timer(3, prescaler=0, period=0xFFFF)     # Initialize timer 3 on the Nucleo
            
            encoder = EncoderDriver(Apin,Bpin,tim,debug=False) # Construct an EncoderDriver object
        '''
        ## Pin attribute for (1/2) Hall sensors on encoder.
        self.pin_A = pin_A
        
        ## Pin attribute for (2/2) Hall sensors on encoder.
        self.pin_B = pin_B
        
        ## Instance of timer attribute.
        self.timer = timer
        
        ## Timer channel attribute initialized to pin_A.
        self.timer_ch1 = self.timer.channel(1,pyb.Timer.ENC_A, pin=self.pin_A)
        
        ## Timer channel attribute initialized to pin_B.
        self.timer_ch2 = self.timer.channel(2,pyb.Timer.ENC_B, pin=self.pin_B)
        
        ## The most current 'tick' of the encoder position.
        self.current_tick = 0
        
        ## Second most recent recorded encoder position.
        self.position_old = 0
        
        ## Most recent recorded encoder position.
        self.position_new = 0
        
        ## The count difference between position_new and position_old.
        self.delta = 0
        
        ## The debug state (bool).
        self.debug = debug
        
        if self.debug: # When debug == true
            print('Creating an Encoder Driver Object')
        
        
    def update(self):
        '''
        @brief    Corrects under/overflow for the encounter count and advances
                  delta, most recent position, and second most recent position.
        @details Corrects for under/overflow by adding or substracting 65535 from
                 the period of the encoder (16-bit) when a bad delta is detected.
                 For accurate readings, the EncoderDriver object should be called at
                 constantly at regular intervals.
        @code Example:

            interval = 20 # Interval of 20 ms
            start_time = pyb.millis()
            next_time = interval + pyb.elapsed_millis(start_time) 
            
            while True:
                if (pyb.elapsed_millis(start_time) >= next_time): # Check if the time has exceeded the "scheduled" timestamp
                    EncoderDriver.update()   # Update the encoder
                    next_time += interval    # Update the "scheduled" timestamp
        '''
        
        self.delta = self.timer.counter() - (self.position_new % 0xFFFF) # Difference between last and current position
        if self.debug: # When debug == true
            print('tim.counter: ' + str(self.timer.counter()))
            print('position_new % 0xFFFF: ' + str(self.position_new % 0xFFFF))
            print('\n')
        if self.delta > 0xFFFF/2: # Check for underflow
            self.delta -= (0xFFFF + 1)                    # Fixes underflow (for 16-bit encoder)
            
        elif self.delta < -0xFFFF/2: # Check for overflow
            self.delta += (0xFFFF + 1)                    # Fixes overflow (for 16-bit encoder)
            
        else:
            pass
        
        self.position_old = self.position_new               # Advance second most recent encoder position
        self.position_new  = self.position_old + self.delta # Advance most recent coder position
        
        if self.debug: # When debug == true
            print('Encoder Position: ' + str(self.position_new))
            print('Encoder Delta: ' + str(self.delta))
    
    
    def get_position(self):
        '''
        @brief   Returns the absolute value of the encoder position since initialization
                 or zero-ing -- accounting for over/underflow.
        @details For most accurate results, update the encoder before getting the position.
        @return  The most recent value of the encoder position.
        @code Example:
            
            EncoderDriver.update() # Update the most recent position
            current_position = EncoderDriver.get_positon() # Returns the most recent position
        '''
        return self.position_new
    
    
    def get_old_position(self):
        '''
        @brief   Returns the second most recent absolute value of the encoder position since initialization
                 or zero-ing -- accounting for over/underflow.
        @return  The second most recent value of the encoder position.
        @code Example:
            
            position1 = EncoderDriver.get_position()
            EncoderDriver.update()
            position2 = EncoderDriver.get_old_position
            
            print(position1 == position2)
            
            Output: True
        '''
        return self.position_old
        
    
    def set_position(self, setValue):  
        '''
        @brief          Sets all forms of the current encoder position to a specified value.
        @details        For most applications, the encoder should be zeroed by setting
                        the position to 0.
        @param setValue An integer value for the encoder position (count) to be set to.
                        By default, setValue is 0 in order to zero the encoder.
        @code Example:
            
            EncoderDriver.set_position(0) # Will zero the encoder.
        ''' 
        self.position_old = setValue 
        self.position_new = setValue
        self.timer.counter(setValue)
    
    
    def get_delta(self):
        '''
        @brief      Updates and returns the difference in encoder position.
        @details    Is used to caluclate the most up-to-date position of the encoder,
                    as well as the instantaneous velocity.
        @return     The difference between position_new and position_old.
        '''
        return self.delta


'''
---Debug Code--
Run by itself to constantly measure the position, delta, and speed of the encoder.
if __name__ == '__main__':
    # Init Encoder
    Apin1 = pyb.Pin(pyb.Pin.cpu.B4)                 # Construct the encoder pin object (1/2)
    Bpin1 = pyb.Pin(pyb.Pin.cpu.B5)                 # Construct the encoder pin object (2/2)
    tim3 = pyb.Timer(3, prescaler=0, period=0xFFFF) # Construct the timer object
    enc = EncoderDriver(Apin1, Bpin1, tim3, debug = False) # Construct an EncoderDriver object


    interval = 20
    
    start_time = pyb.millis()
    next_time = interval + pyb.elapsed_millis(start_time) 
    
    W_meas = 0

    while True:
        if (pyb.elapsed_millis(start_time) >= next_time):
            # task.update()
            enc.update()
            W_meas = enc.get_delta() * 4 * 10**3 * 60 / (interval * 1000 * 4) # Calculate motor speed (rpm)                                     # Update speed array
            print("W_meas = " + str(W_meas))
        
            next_time += interval # Update the "Scheduled" timestamp
'''