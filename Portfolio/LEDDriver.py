'''
@file  LEDDriver.py

@brief  This file contains a UI task that drives an LED on the Nucleo L476RG

@details  This file implements a finite-state machine that waits for an input from
          a phone app and drives an LED on the Nucleo L476RG board. The LED can only
          be driven at a frequency as fast as the interval speed of the FSM.

@author Grant Gallagher

@date November 10, 2020
'''

import pyb

class LEDDriver:
    '''
    @brief    A finite state machine to control a physical LED.
    @details  This class implements a finite state machine to control the blinking frequency of a 
              physical LED on a Nucleo STM-L476RG development board. The frequency can be
              set to any positive integer value that is less than the interval frequency
              of the FSM. The LED can be turned off by setting the frequency to 0.
    '''
    
    ## Constant defining State 0: Initialization.
    S0_INIT = 0
    
    ## Constant defining State 1: The LED is off.
    S1_OFF  = 1
    
    ## Constant defining State 2: The LED is blinking.
    S2_BLINKING  = 2
     

    def __init__(self, interval):
        '''
        @brief           Constructs an LEDDriver object.
        @param  interval The number of milliseconds between runs of the task.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The duration, in milliseconds, for one cycle of the LED to blink.
        self.period = 0
        
        ## The pin header that is assigned to the physical LED on the Nucleo.
        self.LED = pyb.Pin(pyb.Pin.cpu.A5)

        ## The timestamp for the first iteration.
        self.start_time = pyb.millis()
    
        ## The interval of time, in seconds, between runs of the task.
        self.interval = interval
    
        ## The "timestamp" for when to run the task next.
        self.next_time = self.interval + pyb.elapsed_millis(self.start_time)
        
        
    def run(self):
        '''
        @brief    Runs one iteration of the task.
        @details  The task can be one of three various states: initialization,
                  LED off, or LED blinking. When the LED is blinking, it is set
                  to a square waveform (high or low) with user-inputted desired frequency.
        '''  
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if (pyb.elapsed_millis(self.start_time) >= self.next_time):
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.LED.low()  # LED off
                self.period = 0 # LED off
                self.transitionTo(self.S1_OFF) # Transition to State 1.     
                   
  
            elif(self.state == self.S1_OFF):
                # Run State 1 Code
                self.LED.low() # LED off
                if self.period > 0: 
                    self.transitionTo(self.S2_BLINKING) # Transition to State 2

                       
            elif(self.state == self.S2_BLINKING):
                # Run State 2 Code 
                if self.period == 0:
                    self.transitionTo(self.S1_OFF) # Transition to State 1
                elif (pyb.elapsed_millis(self.start_time)) % self.period < self.period/2:  # First half of period
                    self.LED.high() # LED on
                elif (pyb.elapsed_millis(self.start_time)) % self.period >= self.period/2: # Second half of period
                    self.LED.low()  # Led off
        
                    
            else:
                # Error handling
                pass
            
            self.next_time += self.interval # Update the "Scheduled" timestamp
            
    
    def transitionTo(self, newState):
        '''
        @brief Updates the current state variable.
        @details When called, transitions from the current state of the FSM
                 to the next state.
        @param newState The state in the FSM to transition to.
        '''
        self.state = newState
        
    def setFrequency(self, frequency):
        '''
        @brief            Sets the blinking frequency of the LED.
        @param  frequency The frequency, in Hz, of the LED blinking pattern.
        @code Example:
            LEDDriver.setFrequency(10) # sets the frequency of the led to 10
        '''
        if frequency > 0:                  # Positive frequency
            self.period = 1000 / frequency # Set period to inverse of frequency (in milliseconds)
        else:                              # Non-positive frequency
            self.period = 0                # Set period to zero (including invalid inputs)
            