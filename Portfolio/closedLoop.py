'''
@file  closedLoop.py

@brief  This file contains a closed-loop controller class.

@details  This class is used to calculate one iteration of closed-loop feeedback control.
The controller utilizes proportional, integral and derivative gain. To implement closed-loop control
appropriatle, the method update() should be called at regular intervals, along with updated encoder
velocities. Note: For best results, use a small value of Kd. 

@package Lab0x06
@package Lab0x07

@author Grant Gallagher

@date December 4, 2020
'''
class ClosedLoop:
    '''
    @brief      Utilize PID control to calculate the necessary level output.
    
    @details    This class utilized PID control to apply accurate and responsive correction
                to a motor speed controller. The main function of this class, update(), runs one iteration
                to determine the error between the measured and actual speed of a motor, and return
                a level value that should be sent to the motor.
    '''
    def __init__(self, Kp, Ki, Kd, interval, debug):
        '''
        @brief Constructs a ClosedLoop object.
        @param Kp A positive number for proportional gain.
        @param Ki A positive number for integral gain.
        @param Kd A positive number for derivative gain.
        @param debug A boolean value that determines debugging state.
        @param interval The interval between updates, used to calculated Integral and Derivative error.
        '''
        ## The gain value for proportional control.
        self.Kp = Kp
        
        ## The gain value for integral control.
        self.Ki = Ki
        
        ## The gain value for derivative control.
        self.Kd = Kd
        
        ## The time, in microseconds, between task updates.
        self.interval = interval
        
        ## The error between measured and desired motor speed.
        self.W_err = 0
        
        ## The motor speed error from the last iteration of ClosedLoop used for derivative control.
        self.W_err_prev = 0
        
        ## The proportional error, from Kp gain.
        self.P_err = 0
        
        ## The integral error, from Ki gain.
        self.I_err = 0
        
        ## The derivative error, from Kp gain.
        self.D_err = 0
        
        ## The debug state (bool).
        self.debug = debug
        
        ## The level (%/rpm) sent to the motor driver.
        self.level = 0
        
        ## The previous level (%/rpm) sent to the motor driver.
        self.level_old = 0
        
        if self.debug: # When debug == true
            print('Creating a ClosedLoop Object')
        
        
    def update(self, W_ref, W_meas):
        '''
        @brief             Runs one iteration PID control the is used to determine motor level.
        @param W_ref       The reference (desired) motor speed.
        @param W_meas      The actual speed of the motor measured from an encoder.
        @return self.level The level, or duty cycle, that should be sent to the motor
                           to correct for the closed-loop error. 
        '''   
        
        self.W_err = (W_ref - W_meas) # Calculates the error      (proportional)
        
        
        self.P_err = self.Kp * self.W_err
        self.I_err += (self.Ki * self.W_err)# * (self.interval / 10**3)
        self.D_err = self.Kd * (self.W_err - self.W_err_prev)# / (self.interval / 10**3)
        
        self.level = self.P_err + self.I_err + self.D_err # + self.level_avg
        
        self.W_err_prev = self.W_err # Updates the previous error (derivative)
        
        # Max motor duty set to 70 to avoid stalling. (Too much load from belts)       
        if self.level > 100:
            self.level = 100
                    
        elif self.level < -100:
            self.level = -100
            
        elif self.level < 0 and W_ref > 0:
            self.level = 0
            
        elif self.level > 0 and W_ref < 0:
            self.level = 0
                        
        else:
            # Error handling
            pass
        if self.debug: # When debug == true
            print("Closed Loop Error: " + str(self.W_err))
            print("Level: " + str(self.level))

        return int(self.level) # Returns the level
    
    def get_Kp(self):
        '''
        @brief     Gets the proportional value of the controller.
        @return Kp The Kp gain (float).
        '''
        return self.Kp
    
    def set_Kp(self, val):
        '''
        @brief      Sets the proportional gain value of the controller.
        @return val The Kp gain (float).
        '''
        self.Kp = val
        
    def get_Ki(self):
        '''
        @brief     Gets the integral gain value of the controller.
        @return Ki The Ki gain (float).
        '''
        return self.Ki
    
    def set_Ki(self, val):
        '''
        @brief      Sets the integral gain value of the controller.
        @return val The Ki gain (float).
        '''
        self.Ki = val
        
    def get_Kd(self):
        '''
        @brief      Gets the derivative gain value of the controller.
        @return Kd The Kd gain (float).
        '''
        return self.Kd
    
    def set_Kd(self, val):
        '''
        @brief     Sets the derivative gain value of the controller.
        @param val The Kd gain (float).
        '''
        self.Kd = val
        
    def set_interval(self, val):
        '''
        @brief     Sets the interval duration of the controller.
        @param val The interval, in ms, between iterations (float).
        '''
        self.interval = val