## @file mainpage.py
#  Brief doc for mainpage.py
#  
#  Detailed doc for mainpage.py 
#  
#  @mainpage
#
#  @section sec_intro Introduction
#  Welcome! This html website serves as documentation for code developed by
#  Grant Gallagher in ME305: Intro to Mechatronics - taught in conjuction by Charlie Refvem and Eric Espinoza-Wade.
#  
#  @section sec_details Portfolio Details
#  The following sections represent my portfolio. See individual modules for details. \n \n
#  \b Modules:
#  - @ref lab0x01
#  - @ref hw0x00
#  - @ref lab0x02
#  - @ref lab0x03
#  - @ref lab0x04
#  - @ref lab0x05
#  - @ref lab0x06
#  - @ref lab0x07
#
#  @section respository Source Code Repository
#  Here is a complete repository containing the source code of each module:
#  https://bitbucket.org/gpgallag/me305/src/master/ \n \n
#  Alternatively, you can find the source code (and documentation) for each module
#  at the bottom of their respective page under @ref sec_details.
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x01 Lab 0x01: Fibonacci Function
#
#  @image html Fibonacci.png "Italian Mathematician, Leonardo Fibonacci"
#  @image latex Fibonacci.eps  "Italian Mathematician, Leonardo Fibonacci" width=10cm
#  @section sec_lab0x01 Summary
#  The Fibonacci sequence is a series of numbers, where the next index is equal
#  the addition of the last indices, starting with 0. (ex. {0, 1, 1, 2, 3, 5, 8...} )
#  Fibonacci numbers appear unexpectedly often in mathematics, so much so that
#  there is an entire journal dedicated to their study, the Fibonacci Quarterly.
#  Applications of Fibonacci numbers include computer algorithms such as the Fibonacci
#  search technique and the Fibonacci heap data structure, and graphs called Fibonacci
#  cubes used for interconnecting parallel and distributed systems.
#  This package includes a single function that is used to calculate the value of
#  any Fibonacci numbe based on an inputted index value, which must be a positive integer.
#  A link to more information on Fibonacci numbers can be found at
#  https://en.wikipedia.org/wiki/Fibonacci_number.
#  
#  @section lab0x01_documentation Documentation
#  Here is the documentation of this lab package: Lab0x01
#  @section lab0x01_source Source Code
#  Here is the link to the repository for the source code this lab package: <b>https://bitbucket.org/gpgallag/me305/src/master/Lab0x01/</b>
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page hw0x00 Hw 0x00: Elevator Finite State Machine
#  @section sec_hw0x00 Summary
#  A finite-state machine (sometimes called a finite state automaton) is a computation
#  model that can be implemented with hardware or software and can be used to simulate
#  sequential logic and some computer programs. This implemenatation of a finite-state-mation
#  includes an imaginary elevator (hardware) that goes between two floors. In the
#  simulation, there are two elevator button inputs that are used to direct the
#  state of the elevator motor - and subsequently the elevator position. The finite-state
#  machine simulation also utilizes two sensors that reflect the position of the
#  elevator. 
#  The state stransition diagram of @ref elevator.py is shown below.
#  @image html StateDiagram.png "Elevator State Transition Diagram"
#  @image latex StateDiagram.eps  "Elevator State Transition Diagram" width=10cm
#  More information on finite-state machines can be found at
#  https://en.wikipedia.org/wiki/Finite-state_machine.
#
#  @section hw0x01_documentation Documentation
#  Here is the documentation of this lab package: Hw0x01
#  @section hw0x01_source Source Code
#  Here is the link to the repository for the source code this lab package:: https://bitbucket.org/gpgallag/me305/src/master/Homework0x01/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x02 Lab 0x02: LED Finite State Machine
#  @section sec_lab0x02 Summary
#  The objective of this section is to demonstrate multitasking and implementation
#  of hardware by creating finite state machines that are used to control a virtual
#  and physical LED onboard the Nucleo STM L476RG. The first task is blinking a
#  "virtual" LED by printing "LED OFF" and "LED ON" on a fixed interval to the
#  console of the Nucleo. The second task is changing the physical LED brightness
#  on the Nucleo L476RG. More specifically, the on-board LED should switch between
#  the following patterns below.
#  @image html ledShape.png "Physical LED Pattern"
#  @image latex ledShape.eps  "Physical LED Pattern" width=5cm
#  One output pattern is a sinusoid and the other output is a sawtooth. Both
#  waveforms have a period of 10 seconds. The final output alternates between the
#  the two patterns every 30 seconds. Additionally, the script runs locally on
#  the Nucleo device. As a result, Spyder does not recognize the imported pyb class
#  and the script does not run properly in the compiler.
#  More information on MicroPython and the imported pyb class is found at:
#  https://docs.micropython.org/en/latest/library/pyb.html
#
#  @section lab0x02_documentation Documentation
#  Here is the documentation of this lab package: Lab0x02
#  @section lab0x02_source Source Code 
#  Here is the link to the repository for the source code this lab package:: https://bitbucket.org/gpgallag/me305/src/master/Lab0x02/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x03 Lab 0x03: Encoder Driver
#  @section sec_lab0x03 Summary
#  The main objective of this section is to write a finite-state machine program
#  that constantly updates the current position of an encoder. The encoder uses
#  two hall-effect sensors in quadriture that can store 16-bits. The program updates
#  the previous and current position of the encoder at regular intervals to calculate
#  the position delta. The position delta is constantly summed in order to calculate
#  the absolute encoder position while avoiding underflow and overflow. Two seperate
#  classes are used to run the encoder task: An encoder driver object that is solely
#  focussed methods to interact with the hardware, and an encoder task object that 
#  uses a finite-state machine to constantly update and interact with the encoder
#  paramters. \n \n
#  The second objective of this section is to develop a non-blocking user interface
#  that allows the user to interact with the encoder in a few ways: Zero the encoder
#  position, retrieve the encoder position, retrieve the encoder delta, or print
#  the list of availbale commands. This user interface is ran asynchronously with
#  the encoder task object. To achieve this goal, the user interface uitilizes UART - 
#  a Universal Asynchronous Receiver-Transmitter. This duplex serial communication
#  bus interacts with a series of variables that are shared between the encoder
#  task and user interface task. This allows for both the encoder task and user
#  interface task to run asynchronously and achieve better encoder accuracy.
# 
#  @section encoderTask Encoder Finite-State Machine
#  Here is a visual illustration of the FSM transition diagram
#  for the encoder task that is used to update the encoder.
#  @image html EncoderStateDiagram.png "Encoder Task State Transition Diagram"
#  @image latex EncoderStateDiagram.eps  "Encoder Task State Transition Diagram" width=10cm
#
#  @section userTask User Interface State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram
#  for the user interface task that is used to interact with the shared encoder variables.
#  @image html EncoderUIStateDiagram.png "UI State Transition Diagram"
#  @image latex EncoderUIStateDiagram.eps  "UI State Transition Diagram" width=10cm
# 
#  @section lab0x03_documentation Documentation
#  Here is the documentation of this lab package: Lab0x03
#  @section lab0x03_source Source Code 
#  Here is the link to the repository for the source code this lab package:: https://bitbucket.org/gpgallag/me305/src/master/Lab0x03/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x04 Lab 0x04: Encoder User Interface
#  @section sec_lab0x04 Summary
#  
#  The main objective of this section is to demonstrate interactivity with the Nucleo
#  through a user interface based in Spyder. To previously interact with the Nucleo,
#  the user must boot up PuTTY and interact with the device via the REPL. This restricted
#  any other forms of communication with the device and proved tedious. The use a user
#  interface through UART proves to be much more user-friendly. \n \n
#  Additionally, This section also integrates other tools that prove useful with
#  sensor integration. Objects such as arrays and comma-seperated value files (.csv)
#  are powerful forms of data storage that allow for easy manipulation and transfer, while
#  the matplotlib library also allows the user to showcase and and analyze the data.
#  More information on the matplotlib library can be found here: https://matplotlib.org/users/index.html.
#
#  @section userInterface User Interface State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram
#  for the user interface task that is used to interact with the user and data collection task.
#  @image html UIStateDiagram.png "User Interface State Transition Diagram"
#  @image latex UIStateDiagram.eps  "User Interface State Transition Diagram" width=10cm
#
#  @section dataCollect Data Collection State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram
#  for the data collection task that is used interact with the encoder and user interface.
#  @image html DataCollectStateDiagram.png "Data Collection State Transition Diagram"
#  @image latex DataCollectStateDiagram.eps  "Data Collection Transition Diagram" width=10cm
#
#  @section UserInterfaceTaskDiagram User Interface Task Diagram
#  Here is a visual illustration of the task diagram between the UserInterface
#  and DataCollect tasks.
#  @image html UITaskDiagram.png "User Interface Task Diagram"
#  @image latex UITaskDiagram.eps  "UserInterface Task Diagram" width=10cm
#
#  @section UserInterfaceOutput Expected Output
#  Here is a sample output of program after being executed in the Spyder console.
#  As seen below, the UserInterface task was ran for a duration of approx. 9 seconds
#  before being terminated by the user
#  @image html encoder_plot.png
#  @image latex encoder_plot.eps width=10cm
#  On the vertical axis is the angular position of the encoder in degrees. On the
#  horizontal axis is the time in seconds. The encoder was turned clockwise, then
#  counter clockwise, the briefly clockwise again before stopping. Along with the plot
#  is a .csv file which contains all of the raw data use to produce the plot. It is
#  crucial to note that the position output is calibrated for the specific motor/encoder
#  used in ME305. This motor has 7 pole pairs. Using this information, the number of 'ticks'
#  can be converted into angular displacement through a simple conversion factor.
# 
#  @section lab0x04_documentation Documentation
#  Here is the documentation of this lab package: Lab0x04
#  @section lab0x04_source Source Code 
#  Here is the link to the repository for the source code this lab package:: https://bitbucket.org/gpgallag/me305/src/master/Lab0x04/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x05 Lab 0x05: BLE User Interface
#  @section sec_lab0x05 Summary
#  
#  The main objective of this section is to interact with the Nucleo L476RG via
#  a Bluetooth Low Energy (BLE) wireless device. Disclaimer: The BLE module used
#  to develop this script is the HM-11 from DSD Tech. Previously, the only form of interactivity
#  with the Nucleo was via the UART in Putty. While using Putty, the user has no method of
#  uploading code, or interacting with the Nucleo unless through the REPL window. With
#  the development of a wireless user interface, the user can now access the Nucleo
#  (and interact with any script) via more friendly means, like smartphone apps
#  or GUIs. \n\n
#  To showcase this interactivity, this program waits for an input frequency from
#  a smartphone application and controls the blinking frequency of an LED on-board
#  the Nucleo. When a new value is received the LED should immediately blink at the
#  desired frequency. When a value cannot be interpretted as a interprettable frequency (e.g. string, float
#  negative number) it is rejected. The program then gives feedback to the user
#  by displaying the course of action taken. If an unnacceptable value is inputted, 
#  an error message will be displayed. When a value of 2 is inputted, the phone will display
#  "LED blinking at 2Hz."
#
#  @section BLEDriver Bluetooth Low Energy (BLE) Driver State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram
#  for the BLE Driver (user interface) task that interacts with the user and LED driver.
#  @image html BLEDriverStateDiagram.png "BLE Driver State Transition Diagram"
#  @image latex BLEDriverStateDiagram.eps  "BLE Driver State Transition Diagram" width=10cm
#
#  @section LEDDriver LED Driver State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram
#  for the LED Driver task that is used to toggle the state of the LED on the Nucleo.
#  @image html LEDDriverStateDiagram.png "LED Driver State Transition Diagram"
#  @image latex LEDDriverStateDiagram.eps  "LED Driver Transition Diagram" width=10cm
#
#  @section LEDUITaskDiagram LED User Interface Task Diagram
#  Here is a visual illustration of the task diagram between the phone, BLE Driver, 
#  LED Driver, and physical LED.
#  @image html LEDUITaskDiagram.png "LED User Interface Task Diagram"
#  @image latex LEDUITaskDiagram.eps  "LED User Interface Task Diagram" width=10cm
# 
#  @section lab0x05_app Thunkable App
#  Here is the source location for the Thunkable App: https://x.thunkable.com/projects/5fac34a60baa880011568903
#  @section lab0x05_documentation Documentation
#  Here is the documentation of this lab package: Lab0x05
#  @section lab0x05_source Source Code 
#  Here is the link to the repository for the source code this lab package:: https://bitbucket.org/gpgallag/me305/src/master/Lab0x05/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x06 Lab 0x06: DC Motors
#  @section sec_lab0x06 Summary
#  The main objective of this section is to create a closed-loop feedback control
#  system that can be used to regulate the response of a DC motor. To begin, a motor driver
#  class was constructed to allow for direct control of the motor using PWM. With the motor driver class
#  created, a closed loop control function was then created to measure the error in
#  motor speed (compared to desired) and assign a corrective level.
#  To test the collective program and compare different gain values, the motor
#  is 'stepped' to measure how the system behaves.
#
#  @section Control Closed-loop Feedback and PID Control
#  A fixed PWM duty signal to a motor is not an accurate method in controlling
#  the speed or even position of the motor. Interferences, such as friction, electrical
#  noise, vibrations, and external torques can hinder the performance an interfere with
#  a motor - a motor with a load will react differently than a motor without a load under
#  the same PWM duty cycle. To account for these interferences, we can implement closed-loop
#  control, which actively monitors the behavior of the system in real-time and 
#  compares the measured values to to the desired values (error). A *simple* block diagram
#  illustration of the lab motor setup can be seen below.
#  @image html ControlBlockDiagram.png "Simplified Motor Closed-loop Feedback Block Diagram"
#  @image latex ControlBlockDiagram.eps  "Simplified Motor Closed-loop Feedback Block Diagram" width=10cm
#  In this case, PID control is implemented to determine the desired output level from the
#  error in motor speed. PID stands for proportional, integral, and derivative. To put it simply,
#  a PID gain amplyifies (or attenuates) the measured error. Typically, larger gain values
#  can make the system more responsive - at the cost of overshoot, increased settling
#  time, and in some cases instability. Gain values that are smaller can reduce or even
#  eliminate overshoot, though the cost of increased settling time or even steady-state
#  error. Well-balanced gains can help minimize all of these unwanted characteristics
#  and yeild desireable outcomes. More information on PID control can be found here:
#  https://en.wikipedia.org/wiki/PID_controller.
#
#  @section SystemTaskDiagram System Task Diagram
#  An illustration of the task diagram used to control, step, and record the motor,
#  as well as the user interface, is found below.
#  @image html ControllerTaskDiagram.png "System Task Diagram"
#  @image latex ControllerTaskDiagram.eps  "System Task Diagram" width=10cm
#  The InterfaceFront task is ran locally on a PC, through a
#  compiler such as Spyder, to send gain values and reference speeds to the Nucleo
#  (and thus the controller). The InterfaceRemote task is ran remotely on the Nucleo
#  device to receive commands from the InteraceFronttask via a UART. Alternateivly,
#  the back-end of the UI assigns the gains and motor reference speed via a shares,
#  which the TaskControl (or controller) can use. Once the user sends the command to
#  start stepping the motor, the controller periodically measures the encoder speed
#  from the EncoderDriver and updates one iteration of ClosedLoop control to calcualat
#  the necessary level to send to the MotorDriver. All-the-while, TaskControl calculates
#  and stores the motor speed (rpm) and the respective timestamps to send back to
#  the InterfaceFront through InterfaceRemote. Finally, the motor speeds and
#  timestamps from the step response are exported to the user as a .png plot and
#  a .csv file.
#
#  @section InterfaceFront Front-end User Interface State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram
#  for the Front-end UI task that is ran locally on a PC which the user interacts with.
#  @image html InterfaceFrontStateDiagram.png "InterfaceFront State Transition Diagram"
#  @image latex InterfaceFrontStateDiagram.eps  "InterfaceFront Transition Diagram" width=10cm
#
#  @section InterfaceRemote Back-end User Interface State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram
#  for the Back-end UI task that is ran remotely on the Nucleo that coordinates user commands.
#  @image html InterfaceRemoteStateDiagram.png "InterfaceRemote State Transition Diagram"
#  @image latex InterfaceRemoteStateDiagram.eps  "InterfaceRemote Transition Diagram" width=10cm
#
#  @section TaskControl Closed-loop Controller State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram
#  for the Controller task that steps the motor and periodically corrects for error.
#  @image html TaskControlStateDiagram.png "TaskControl State Transition Diagram"
#  @image latex TaskControlStateDiagram.eps  "TaskControl Transition Diagram" width=10cm
#
#  @section MotorStep Motor Step Response Plots and Gain Tuning
#  It is important to mention that all motor step-response plots are created
#  using the output shaft speed, *not* the motor speed. That is, these are the
#  values are for the shafts located on the outside of the board (the output of
#  the system). The motor speed should be approximately four times greater
#  do to the ratio between pulleys. With that being said, the reference speed
#  for each of the step tests is a moderate 700 RPM, a little over half the
#  maximum output speed of the system. Seen below is a plot of the first
#  motor step response with a Kp value of 0.2.
#  @image html Kp_0.2.png
#  @image latex Kp_0.2.eps width=10cm 
#  The system has a decent rise time of ~100ms, but a lot of steady-state error.
#  The reference speed was set to 700 RPM, and the system is holding a *rough*
#  480 RPM. By increasing the proportional gain, the system should have less 
#  steady state error.
#  @image html Kp_0.4.png
#  @image latex Kp_0.4.eps width=10cm 
#  By doubling the proportional gain to Kp=0.4, the measured speed now holds
#  around 580 RPM. By increaing the proportional gain even more, the steady state
#  error should go down even more.
#  @image html Kp_0.8.png
#  @image latex Kp_0.8.eps width=10cm 
#  With Kp=0.8, the motor speed has increased to approximately 640 RPM - a steady
#  state error of 60 RPM or ~9%. A subtle trend that can be noticed when paying
#  close attention to the plots of increasing proportional gains us that the system
#  is becoming more sporadic. A higher gain means that the system is more sensative,
#  and will overcorrect more easily. For the plot of Kp=0.2, the fluctation in
#  output speed was around ±50 RPM. With a gain of Kp=0.8, the speed fluctautes
#  almost ±90RPM. 
#  @image html Kp_4.png
#  @image latex Kp_4.eps width=10cm 
#  With a very large gain of Kp=4, the steady state error is extremely small (
#  imagine an average line in between the fluctating points). The system now
#  hovers around the desired 700 RPM, however, it has become very unstable. Now,
#  the system fluctautes nearly ±150 RPM. One *very* important observation about
#  the system is that there appears to be mechanical defects. It appears that the
#  there is either too much tension in the belt, or the pulleys have a large amount
#  of runout, causing there to be oscillations in the output. When ran at very slow
#  speeds, it's clear to see that the motor struggles to overcome particular sections
#  of the pulley. Nonetheless, implementing full PID control can help reduce
#  (not eliminate) this oscillation.
#  @image html Ki_2.png
#  @image latex Ki_4.eps width=10cm
#  By implementing PI control, the system has become much more stable. Additionally,
#  the steady state error of the system will always go to zero. The speed at which
#  the error goes to zero is largely dependent on the Ki gain. Now, it is more clear
#  to see the overshoot in the step response. The system jumps to a speed of 810 RPM
#  before settling back down to the desired 700 RPM. There is also a bit of oscillation
#  too, which would suggest that the system is overdamped. To help correct for this,
#  one of the gains could be lowered, *or* derivative control can be implemented.
#  Typically, derivative control is very touchy and often left out. However, when
#  used with very low gain values, the derivative control can help reduce the
#  over-corrective behavior of system and improve the response of the system.
#  @image html BestStep.png
#  @image latex BestStep.eps width=10cm
#  While P control is useful, using PID (or even just PI) control is much more
#  powerful for a second-order system such as this. The elimination of steady-state
#  error and attenuating properties of the integral term provides substantial
#  differences. While these results are sufficient, there are two areas of this
#  test that can be improved. First, the runout/friction of the pully system should
#  be fixed so that the motor does not have to struggle every rotation - this is similar
#  to a sinusoidal torque resisting the motor. Second, the results would be *much*
#  smoother if the Nucleo was capable of outputting signals faster than 1ms. For these
#  results, the TaskControl task was ran at an interval of 15ms. The script is set up
#  such that the intervals can be in microseconds; however, the processor was only
#  capable of keeping up speeds less than 1 ms. According to the DCX22S datasheet, the time constant
#  of the motor is 6ms. This would mean that it would be ideal for the task to run
#  at, or less than, around 1ms intervals - or 1/5th of the time constant of the motor.
#
#  @section lab0x06_documentation Documentation
#  Here is the documentation of this lab package: Lab0x06
#  @section lab0x06_source Source Code 
#  Here is the link to the repository for the source code this lab package:: https://bitbucket.org/gpgallag/me305/src/master/Lab0x06/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.
#
#
#  @page lab0x07 Lab 0x07: Reference Tracking
#  @section sec_lab0x07 Summary
#  The main objective of this section is to adapt the code from @ref Lab0x06 to perform
#  reference tracking instead of setpoint control. Please see Lab 0x06, DC Motors, for
#  the system block diagram or more detail on motor control. The reference path of the motor
#  is given in as a CSV file containing three columns of data: the first column contains
#  time in units of seconds, the second column contains angular velocity in units of 
#  RPMS, and the third column contains angular position units of degrees. After unpacking
#  the data, it is sent to the Nucleo so that it can be used as a reference for the
#  motor control.  A visual plot of the reference path is shown below.
#  @image html ReferenceData.png
#  @image latex ReferenceData.eps width=10cm
#  At the beginning of the test, the system is stepped to 30 RPM. Keep in mind, this is
#  a *very* low speed as the system has a maximum speed of around 1200 RPM. At four
#  seconds, the motor stops moving. At seven seconds, the motor spins at 60 RPM in the
#  reversed direction for one second. After eleven seconds, the motor is ramped up
#  to 30 RPM (over one second) and then ramped back down to rest (over one second).
#
#  @section Performance System Performance
#  After the reference tracking is is completed, the system data is sent back to the
#  user-end of the program where the measured data is compared to the reference data.
#  The raw measured data is exported as a CSV file, and the measured positions and
#  velocities are plotted against their reference counterparts. Finally, the performance
#  metric, J, of the system is calculated. Since the controller runs on (relatively large)
#  intervals, the performance metric is discretized and calculated by the following
#  equation
#  @image html PerformanceEq.png
#  @image latex PerformanceEq.eps width=10cm
#  where k is the index of the current iteration of controller and K is the total
#  number of iterations ran over the test. The performance metric, J, calculates how 
#  far off the system is from the desired behavior, and is a good indication
#  of how well the controller works. A large value of J indicates that the system measurement
#  data is very different from the reference data. A small value of J indicates that
#  the system behaves close to expected, and the controller is well-tuned.
#
#  @section SystemTaskDiagram07 System Task Diagram
#  An illustration of the task diagram used to control, track, and record the system,
#  as well as the user interface, is found below.
#  @image html ControllerTaskDiagram07.png "System Task Diagram"
#  @image latex ControllerTaskDiagram07.eps  "System Task Diagram" width=10cm
#  The task diagram, as well as most of the tasks, are very similar to those found
#  in @ref Lab0x06. The primary difference in the task diagram for the reference
#  tracking program is that a profile must be sent to the Nucleo instead of a single
#  reference velocity. To do this, the CSV reference values are read into an array
#  on the front-end of the user interface- on the PC. Then, the data is sent to the
#  Nucleo, piece-by-piece in intervals of 20ms through the UART. Likewise, an array
#  of reference times and velocities are sent to the controller task, where the
#  controller iterates through the profile and sends the desired parameters to the
#  closed loop method. Similar to before, measured and array of angular velocities is
#  sent back to the front end of the UI; however, this time an array of angular positions
#  is also sent. The front end UI then plots and exports the data and calculates the 
#  performance metric.
#
#  @section InterfaceFront07 Front-end User Interface State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram
#  for the Front-end UI task that is ran locally on a PC which the user interacts with.
#  @image html InterfaceFrontStateDiagram07.png "InterfaceFront State Transition Diagram"
#  @image latex InterfaceFrontStateDiagram07.eps  "InterfaceFront Transition Diagram" width=10cm
#
#  @section InterfaceRemote07 Back-end User Interface State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram
#  for the Back-end UI task that is ran remotely on the Nucleo that coordinates user commands.
#  @image html InterfaceRemoteStateDiagram07.png "InterfaceRemote State Transition Diagram"
#  @image latex InterfaceRemoteStateDiagram07.eps  "InterfaceRemote Transition Diagram" width=10cm
#
#  @section TaskControl07 Closed-loop Controller State-Transition Diagram
#  Here is a visual illustration of the FSM transition diagram
#  for the Controller task that steps the motor and periodically corrects for error.
#  @image html TaskControlStateDiagram07.png "TaskControl State Transition Diagram"
#  @image latex TaskControlStateDiagram07.eps  "TaskControl Transition Diagram" width=10cm
#
#  @section ReferenceResponse Reference Tracking Results
#  The reference profile was tracked via the velocity profile with PID control; that is,
#  the controller task determined the error of the iteration by comparing the desired
#  and measured system speed. The parameters used to control the system
#  are as follows: Timing interval = 20ms, Kp = 0.1, Ki = 0.1, and Kd = 0.005. The
#  subplots of the response/reference data is shown below.
#  @image html BestTrack.png "Measured system profile overlayed on reference profile"
#  @image latex BestTrack.eps  "Measured system profile overlayed on reference profile" width=10cm
#  As mentioned previously, the controller was based on system speed, *not position*.
#  As a result, there is a small amount of steady-state error anywhere the reference
#  velocity is zero. Since the reference velocity is zero, the system will not move despite
#  being off from the desired position. Though it would be more accurate to read
#  from both the velocity and position of the reference data, the results shown
#  are accurate enough for the scope of this project. *The performance metric, J,
#  for this run was calculated to be 0.001344.* For all other runs with
#  equivalent gain and sample rates, the performance metric remained below 0.06.
#  This is a good indication the the motor controller is responding well to the
#  error between the measured and reference data. Suggested methods to improve
#  the respons of the system is found in the @ref Issues section.
#
#  @section Issues Issues and Discussion
#  As seen in plot @ref ReferenceResponse sections, the position response is very accurate.
#  The system responds well to both step and ramp inputs. However, the measured velocity
#  response has a lot of oscillations. These oscillations can be a result of a variety of factors,
#  including but not limited to: too large of gain values, mechanical noise, electrical 
#  noise, motor speed, low sample rate, and oscillations. \n\n
#  The most likely reasons for such noise in this instance is the sample rate, motor speed, and 
#  drivetrain resistance. The DCX22S motor that is being used has a time constant of
#  ~ 6ms. Such a motor should be run at 1ms, or less than 1/5th of the time constant.
#  In this case, the motor is being controlled with a timing of 20ms - more than 
#  3 times the time constant. As a result, the motor responds faster than the controller
#  can measure. Additionally, the system is being driven at < 60 RPM, which is approximately
#  1% of its maximum speed. From previous testing, it was determined the the motor does not
#  respond well to duty levels of less than 25% (at 12 V), likely due to torque caused
#  by the pulley. For every revolution of the encoder shaft, the system experiences
#  a very noticeable resistance. From visual inspection, the motor shaft has a moderate
#  amount of runout that would increase the tension in the pulley. While these problems
#  are apparent, their effect on the system is insubstantial for
#  the scope of the project. \n\n
#  Throughout the duration of this section, there were many trials and tribulations.
#  One problem that carried on from lab0x06 was that the optical encoders
#  kept periodically shutting off, leading to heinous velocity readings and non-existant
#  motor control. However, there was an even larger problem that occured during this
#  process - the motor driver chip shut out entirely. After reading every lead between
#  the nucleo and the motor terminals, I narrowed the issue down to the internals of the
#  DRV8847 chip. The PWM and nSleep signals from the Nucleo reached the input pins 
#  on the motor driver chip just fine; however, one side of the output pins of the chip
#  was low (for both M1- and M2- leads). As a result, the motor could only be turned
#  in one direction. As a solution, I grabbed a motor driver PCB that I made in ME507
#  and used that as the new driver. A (albeit rough looking) setup can be seen below
#  @image html Setup.png "Lab0x07 Alternative Setup"
#  @image latex Setup.eps  "Lab0x07 Alternative Setup" width=10cm
#  The purple PCB on top of the Nucleo is the custom PCB that I made, containing
#  connections for a motor, encoder, linear potentiometer, and other GPIO pins.
#  One caveat of this setup was that the PCB use PH/EN mode to control the motor, not
#  PWM mode. As a result, I had to alter the @ref motorDriver.py class to accept
#  this change. Content with the setup, I connected the Nucleo with the purple PCB
#  to the black PCB from the ME405 kit so that it would be neater and I could utilize
#  the additional UART connection. 30 seconds after connecting the boards, the room
#  smelled like plastic and the Nucleo processor became to hot to touch. I likely attribute
#  this problem to a short in the pins between the two PCBs. I tried reflashing the Nucleo
#  with STlink software to no avail. Luckily, a friend of mine gifted me a Nucleo that
#  he had no use of anymore. In total, this ordeal fixed the motor driver issue, but
#  costed nearly 4 days of diagnostics and downtime.
#
#  @section lab0x07_documentation Documentation
#  Here is the documentation of this lab package: Lab0x07
#  @section lab0x07_source Source Code 
#  Here is the link to the repository for the source code this lab package:: https://bitbucket.org/gpgallag/me305/src/master/Lab0x07/
#  - - -
#  @author Grant Gallagher
#  @copyright Copyright © 2020-2021 Grant Gallagher, all rights reserved.