'''
@file main.py
'''
import pyb
from encoderDriver import EncoderDriver
from motorDriver import MotorDriver
from InterfaceRemote import InterfaceRemote
from taskControl import TaskControl
if __name__ == '__main__':
    
    ## Init Motor
    pin_nSleep = pyb.Pin(pyb.Pin.cpu.A9)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B10)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.A8)
    tim2 = pyb.Timer(2, freq = 20000)
    moe1 = MotorDriver(pin_nSleep, pin_IN1, pin_IN2, tim2, debug = False)
    
    # Init Encoder
    Apin1 = pyb.Pin(pyb.Pin.cpu.B4)                 # Construct the encoder pin object (1/2)
    Bpin1 = pyb.Pin(pyb.Pin.cpu.B5)                 # Construct the encoder pin object (2/2)
    tim4 = pyb.Timer(3, prescaler=0, period=0xFFFF) # Construct the timer object
    enc1 = EncoderDriver(Apin1, Bpin1, tim4, debug = False) # Construct an EncoderDriver object

    # Init Control
    interval = 1000
    
    duration = 1000
    
    task = TaskControl(enc1, moe1, 50)
    
    task2 = InterfaceRemote(10)

    while True:
        task.run()
        task2.run()

        
