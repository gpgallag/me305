'''
@file  taskControl07.py

@brief  This file contains a finite-state machine task that repetitvely iterates closed-loop control of a motor.

@details  This task receives a commands from an InterfaceRemote object, via a shares file,
which sets the Ki, Kp, Kd, and motor reference speeds for a motor step response. The task
also receives a "start" signal from the InterfaceRemote task to begin stepping the motor.
Note: This class is not the same as TaskControl which is is found in package 0x06.
Instead of stepping a motor, this class implements reference-tracking control via
a velocity profile using a shared array.

@package Lab0x07

@author Grant Gallagher

@date December 4, 2020
'''
import utime
import shares
from closedLoop import ClosedLoop

class TaskControl07:
    '''
    @brief      Encapsulates the operation of closed-loop PID control of a motor.
    
    @details    This class initializes a ClosedLoop controller object that is used
                to correct the speed and response of a motor so that it follows
                a reference pattern accurately. The class also contains a finite-state
                machine that is used to interact with the back-end of a UI.
                An update task is created to run periodically so that the
                controller can continiously correct for the real motor behavior.
    '''
    ## State 0: Initialization
    S0_INIT = 0
    
    ## State 1: Wait for user input.
    S1_WAIT = 1
    
    ## State 2: Collect encoder data.
    S2_COLLECT_DATA = 2
    
    def __init__(self, encoder, motor, interval):
        '''
        @brief Constructs a TaskControl07 object.
        @param encoder  An EncoderDriver object.
        @param motor    A MotorDriver object
        @param interval The number of microseconds between desired runs of the task.
        @param duration The total amount of time (microseconds) to run the motor step.
        '''
        ## An encoder object.
        self.encoder = encoder
        
        ## An motor object.
        self.motor = motor 
        
        ##  The amount of time in microseconds between runs of the task. 
        self.interval = interval
        
        self.ref_index = 1
        
        self.time_ref = 0
        
        self.W_ref = 0
        
        self.position_ref = 0
        
        ## The level (%/rpm) sent to the motor driver.
        self.level = 0
        
        ## The ratio between the motor and encoder pully (1:4)
        self.gear_ratio = 4
            
        ## The number of counts per revolution for the encoder.
        self.CPR = 4000 # ----------------------------------------------------------------------- Bug
    
        ##  The timestamp for when the task begins collecting motor data.
        self.collecting_start_time = 1
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration.
        self.start_time = utime.ticks_ms()
        
        # The gear ratio between the encoder/motor pulleys
        self.gear_ratio = 4
        
        ## The timestamp for the current task run.
        self.current_time = 0
        
        ## The "timestamp" for when the task should run next.
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ##  A ClosedLoop object that sets the parameters for closed-loop feedback control.
        self.controller = ClosedLoop(shares.Kp, shares.Ki, shares.Kd, self.interval, debug = False)
        
        self.performance_sum = 0
        
        self.performance = 0
    
        
    def run(self):
        '''
        @brief Runs one iteration of the task.
        '''
        ## The timestamp for the current task run.
        self.current_time = utime.ticks_ms()
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.current_time, self.next_time) >= 0):
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.motor.enable()
                # shares.array_time_ref.append(0)
                # shares.array_time_ref.append(200)
                # shares.array_time_ref.append(400)
                # shares.array_time_ref.append(600)
                # shares.array_time_ref.append(800)
                # shares.array_time_ref.append(1000)
                
                # shares.array_W_ref.append(700)
                # shares.array_W_ref.append(700)
                # shares.array_W_ref.append(400)
                # shares.array_W_ref.append(400)
                # shares.array_W_ref.append(900)
                # shares.array_W_ref.append(900)
                # print("State 0.")
                # print("time: " + str(shares.array_time_ref))
                # print("w: " + str(shares.array_W_ref))
                
                self.transitionTo(self.S1_WAIT) # Transition to State 1   
            
            elif(self.state == self.S1_WAIT):
                # Run State 1 Code
                # print("State 1.")
                # shares.collect = True
                # print(shares.collect)
                if shares.collect == True:
                    print("Went into if statement")
                    # print("Taskcollect == True")
                    self.controller.set_Kp(shares.Kp) # Set the Kp gain to the value in shares
                    self.controller.set_Ki(shares.Ki) # Set the Ki gain to the value in shares
                    self.controller.set_Kd(shares.Kd) # Set the Kd gain to the value in shares
                    print("Set gains")
                    self.encoder.set_position(0)
                    self.collecting_start_time = utime.ticks_ms() # Create a timestamp for when the task is started.
                    self.interval = int((shares.array_time_ref[self.ref_index] - shares.array_time_ref[self.ref_index-1]))
                    self.controller.set_interval(self.interval) # Set the Kd gain to the value in shares
                    print("Some calcs")
                    print("Interval: " + str(self.interval) + 'ms')
                    # print("Moving to State 2")
                    # print("Current time: " + str(self.current_time))
                    # print("New Interval: " + str(self.interval))
                    # print("Next time: " + str(self.current_time + self.interval))
                    # print("Length of shares: " + str(len(shares.array_time_ref)))
                    self.transitionTo(self.S2_COLLECT_DATA) # Transition to State 2  
                    print("Transition to Sate 2")
                    
            elif(self.state == self.S2_COLLECT_DATA):
                self.update(self.ref_index)
                print(str(self.ref_index))
                self.ref_index += 1
                
                if (self.ref_index >= len(shares.array_time_ref)):
                    print()
                    self.motor.set_duty(0) # Turn the motor off
                    shares.collect = False # Reset the shares collect variable
                    print(shares.array_time_meas)
                    print(len(shares.array_time_meas))
                    print('\n')
                    print(shares.array_position_meas)
                    print(len(shares.array_position_meas))
                    print('\n')
                    print(shares.array_W_meas)
                    print(len(shares.array_W_meas))
                    self.ref_index = 1
                    self.interval = 20 
                    self.update_performance_metric()
                    self.transitionTo(self.S1_WAIT) # Transition to State 1 
                    
                else:
                    self.interval = int((shares.array_time_ref[self.ref_index] - shares.array_time_ref[self.ref_index-1]))

            else:
                # Invalid state code (error handling)
                pass
            
            self.next_time = utime.ticks_add(self.current_time, self.interval) # Update the "Scheduled" timestamp
                
    def transitionTo(self, newState):
        '''
        @brief Updates the current state variable.
        @param newState The state in the FSM to transition to.
        '''
        self.state = newState
    
    def update(self, index):
        '''
        @brief Runs one iteration of closed-loop control to set the motor duty.
        @details Updates the encoder to measure the angular velocity (rpm). Then, calculates
        the error between desired motor speed and actual motor speeds. Calculates necessary
        level to set the motor speed from ClosedLoop.update().
        '''       
        shares.array_time_meas.append(int(utime.ticks_diff(self.current_time, self.collecting_start_time))) # Update time array
        
        shares.array_position_meas.append(int((self.encoder.get_position() * (360 / self.CPR))))
        
        self.encoder.update() # Updates encoder position
        W_meas = self.encoder.get_delta() * 1000 * 60 / (self.interval * 4000)
        shares.array_W_meas.append(int(W_meas))
    
        self.level = int(self.controller.update(shares.array_W_ref[index], W_meas)) # Calculates level
        print("Desired speed: " + str(shares.array_W_ref[index]) + ' | Actual Speed: ' + str(W_meas) + ' | Time: ' + str((int(utime.ticks_diff(self.current_time, self.collecting_start_time)))))
        print("LEVEL: " + str(self.level))
        if shares.array_W_ref[index] == 0:
            self.motor.set_duty(0)
        else:
            self.motor.set_duty(self.level) # Sets motor speed to level
        
    def update_performance_metric(self):
        '''
        @brief XX
        @details XXXX
        '''
        for index in range(len(shares.array_time_ref)-1):
            self.performance_sum += ( (shares.array_W_ref[index] - shares.array_W_meas[index])**2 + (shares.array_time_ref[index] - shares.array_time_meas[index])**2 )
        self.performance = self.performance_sum / (len(shares.array_time_ref) - 1)
        

if __name__ == '__main__':
    import pyb
    from motorDriver import MotorDriver
    from encoderDriver import EncoderDriver

    ## Init Motor 1
    pin_nSleep = pyb.Pin(pyb.Pin.cpu.A15)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    tim3 = pyb.Timer(3, freq = 20000)
    moe1 = MotorDriver(pin_nSleep, pin_IN1, pin_IN2, tim3, debug = True)
    # Init Motor 2
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)
    tim3 = pyb.Timer(3, freq = 20000)
    moe2 = MotorDriver(pin_nSleep, pin_IN1, pin_IN2, tim3, debug = False)
    
    # Init Encoder 1
    Apin1 = pyb.Pin(pyb.Pin.cpu.B6)                 # Construct the encoder pin object (1/2)
    Bpin1 = pyb.Pin(pyb.Pin.cpu.B7)                 # Construct the encoder pin object (2/2)
    tim4 = pyb.Timer(4, prescaler=0, period=0xFFFF) # Construct the timer object
    enc1 = EncoderDriver(Apin1, Bpin1, tim4, debug = False) # Construct an EncoderDriver object
    # Init Encoder 2
    Apin2 = pyb.Pin(pyb.Pin.cpu.C6)                 # Construct the encoder pin object (1/2)
    Bpin2 = pyb.Pin(pyb.Pin.cpu.C7)                 # Construct the encoder pin object (2/2)
    tim8 = pyb.Timer(8, prescaler=0, period=0xFFFF) # Construct the timer object
    enc2 = EncoderDriver(Apin2, Bpin2, tim8, debug = False) # Construct an EncoderDriver object

    # Init Control
    interval = 10
    
    task = TaskControl07(enc1, moe1, interval)
    shares.collect = True

    while True:
        # enc1.update()
        # print(str(enc2.timer.counter()))
        task.run()