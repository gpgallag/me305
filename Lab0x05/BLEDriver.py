'''
@file  BLEDriver.py

@brief  This file contains a Bluetooth Low Energy (BLE) driver.

@details  This file implements a finite-state machine that drives a BLE wireless module.
          The driver can write to, read from, and check if any characters are waiting on the UART.
          This driver is a finite-state machine user-interface that interacts with
          the LED on the Nucleo L476RG via a smartphone through the UART.

@package Lab0x05

@brief This package contains BLEDriver.py and LEDDriver.py.

@author Grant Gallagher

@date November 10, 2020
'''

import pyb
from pyb import UART

class BLEDriver:
    '''
    @brief    A Bluetooth Low Energy (BLE) driver used to interact with a wireless device.
    @details  This driver is a finite-state machine user-interface that interacts with
              the LED on the Nucleo L476RG via a smartphone through the UART.
              It also is used to control the LEDDriver object.
    '''
    
    ## Constant defining State 0: Initialization.
    S0_INIT = 0
    
    ## Constant defining State 1: Check the UART for an input.
    S1_CHECK_UART  = 1
    
    ## Constant defining State 2: Respond to the UART input.
    S2_RESPOND  = 2

    def __init__(self, interval, debug):
        '''
        @brief          Constructs a BLEDriver object
        @param interval The number of milliseconds between runs of the task.
        @param debug    A boolean value that determines debugging state.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The UART object initialized for smartphone communication.
        self.uart = UART(3, 9600) # FOR APP
        
        ## The user input received via the UART.
        self.val = 0

        ## The timestamp for the first iteration.
        self.start_time = pyb.millis()
    
        ## The interval of time, in seconds, between runs of the task.
        self.interval = interval
    
        ## The "timestamp" for when to run the task next.
        self.next_time = self.interval + pyb.elapsed_millis(self.start_time)
        
        ## The debug state (bool).
        self.debug = debug
        
        
    def run(self, LED):
        '''
        @brief     Runs one iteration of the task.
        @details   The task can be one of three various states: initialization,
                   waiting for an input via the UART, or respond to input.
                   The FSM can interact with an LEDDriver object by controlling
                   the blinking frequency.
        @param LED An LEDDriver object that is to be controlled.
        '''  
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if (pyb.elapsed_millis(self.start_time) >= self.next_time):
            
            if(self.state == self.S0_INIT):
                # Run State 0 
                self.uart.write("Initialized.")
                self.uart.write("Please enter a positive integer to set the blinking frequency of the LED on the Nucleo.")
                
                if self.debug == True:
                    print("Initialized.")
                    print("Please enter a positive integer to set the blinking frequency of the LED on the Nucleo.")
                
                self.transitionTo(self.S1_CHECK_UART) # Transition to State 1          
                   
  
            elif(self.state == self.S1_CHECK_UART):
                # Run State 1 Code
                if self.uart.any() != 0:               # Input detected
                    self.val = self.uart.readline()    # Store input as self.val
                    self.transitionTo(self.S2_RESPOND) # Transition to State 2
                    
            elif(self.state == self.S2_RESPOND):
                # Run State 3 Code       
                if (not str(self.val.decode("utf-8")).isdigit() or int(self.val) < 0 ): # Checks for string/negative/integer input
                    self.uart.write("Error. Input must be a positive integer.")
                    
                    if self.debug == True:
                        print("Error. Input must be a positive integer.")
                    
                elif int(self.val) == 0: # Checks for "0" input
                    self.uart.write("LED off.")
                    LED.setFrequency(int(self.val)) # Turn the LED off
                    
                    if self.debug == True:
                        print("LED off.")
                    
                elif int(self.val) >= 1: # Checks for positive integer input
                    self.uart.write("LED blinking at " + str(self.val.decode("utf-8")) + "Hz.")
                    LED.setFrequency(int(self.val)) # Set the LED frequency to input value
                    
                    if self.debug == True:
                        print(("LED blinking at " + str(self.val.decode("utf-8")) + "Hz."))
                    
                else: # Unrecognized input
                    self.uart.write("Error. Unidentifiable input.")
                    
                    if self.debug == True:
                        print("Error. Unidentifiable input.")
                    
                self.transitionTo(self.S1_CHECK_UART) # Transition to State 1 
                    
            else:
                # Error handling
                pass
            
            self.next_time += self.interval # Update the "Scheduled" timestamp
            
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run.
        '''
        self.state = newState