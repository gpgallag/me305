'''
@file  main.py

@brief  This is the main file to initalize and run the LED/BLE tasks indefinitely.

@author Grant Gallagher

@date November 10, 2020
'''

from LEDDriver import LEDDriver
from BLEDriver import BLEDriver

if __name__ == '__main__':
    
    ## An LEDDriver object initialized to a running frequency of 100hz
    LED_task = LEDDriver(10)
    
    ## An LEDDriver object initialized to a running frequency of 10hz
    BLE_task = BLEDriver(100, debug = True)
    
    while True: # Forever
        
        LED_task.run() # Run LED task
        
        BLE_task.run(LED_task) # Run BLE task (controlling LED task)