'''
@file fibonFunction.py

Documentation for use of fibonFunction.py

@brief This file contains a single method, fib(), that calculates the Fibonacci sequence according to a user-provided
       index.
@details This file is a stande-alone file, meaning it can (and should) be run entirely by itself
         in a compiler. When ran, the fib() function will immediately be called. The program will
         prompt the user to enter an index for a fibonacci number, and calculate it
         (note that the Fibonacci sequence begins at index n = 0). The program will also scan
         for valid inputs, which include positive integers. The program will prompt the user if they
         would like to continue with another number or stop.

@package Lab0x01
@brief This module contains fibonFunction.py.

@details This package contains the method fib() which is used to calculate the Fibonacci number at a user specified index.


@author Grant Gallagher

@date September 29, 2020
'''

def fib(idx):
    '''
    @brief      Calculates the fibonacci number
    @details    Accepts an index value as an argument and returns the value of the
                equivalent Fibonacci number at the specified index. For example, fib(10)
                will print the number '55' and fib(-2.1) will print an error message.
    
    @param idx A positive integer specifying the index of the desired
               Fibonacci number.
    '''
    #  Checks the input, idx, for a non-positive integer value and
    #  prompts error message if false
    if (not isinstance((idx), int) or idx < 0 or isinstance(idx, bool)):
       print('Error. The index value must be an integer that is '
             'greater than or equal to zero.')
    
    #  Calculates Fibonacci number at the index n with recursion
    else:
        print('Calculating Fibonacci number at '
              'index n = {:}'.format(idx))
        
        ## An integer that temoporarily holds the value
        #  of the Fibonacci number at the nth index.
        f_n = 0
        
        ##  An integer that temoporarily holds the value
        #  of the Fibonacci number at the (n+1)th index.        
        f_np = 1
        # Recursive loop that determines the next Fibonacci value until
        # Reaching the index
        for j in range(idx):
            temp = f_np
            f_np = f_n + f_np
            f_n = temp
        
        #  Answer
        print("Answer: " + str(f_n))

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
if __name__ == '__main__':

    ## The input from the user. By default, the value is "y" so that the function starts.
    user_in = "y"

    # While loop that continues to prompt user for index unless they state no
    while user_in != "n":   
    
        # Response to saying YES to trying more numbers
        if user_in == "y":
            ## The index for which the user wants to calculate the fibonnaci number of.
            ind = input("Please enter the desired index for the Fibonacci Number:     ")
        
            # Test input for int. Call fib() if valid
            try:
                ind = int(ind)
                fib(ind)
                # Response if input is not int.
            except ValueError:
                print('Error. The index value must be an integer that is '
                      'greater than or equal to zero.')
            user_in = input("Would you like to try another number? (y)es | (n)o:     ")
        
            # Response to saying NO to trying more numbers
            if user_in == "n":
                print("Thank you. Have a good day!")
        
        
        # Response to an INVALID entry.
        else:
            user_in = input("Invalid Entry. Please enter (y) for yes, or (n) for no:     ")
