'''
@file main.py

@author Grant Gallagher

@copyright Copyright 2020, Grant Gallagher, All rights reserved.

@date September 29, 2020

This files is used to run, test, and debug the fibonFunction.fib() method.
'''

import fibonFunction

# Default input prompt entering an index.
user_in = "y"

# While loop that continues to prompt user for index unless they state no
while user_in != "n":   
    
    # Response to saying YES to trying more numbers
    if user_in == "y":
        ind = input("Please enter the desired index for the Fibonacci Number:     ")
        
        # Test input for int. Call fib() if valid
        try:
            ind = int(ind)
            fibonFunction.fib(ind)
        # Response if input is not int.
        except ValueError:
            print('Error. The index value must be an integer that is '
              'greater than or equal to zero.')
        user_in = input("Would you like to try another number? (y)es | (n)o:     ")
        
        # Response to saying NO to trying more numbers
        if user_in == "n":
            print("Thank you. Have a good day!")
        
        
    # Response to an INVALID entry.
    else:
        user_in = input("Invalid Entry. Please enter (y) for yes, or (n) for no:     ")

