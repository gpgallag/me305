'''
@file main.py

@brief This file contains a finite-state machine to run a DataCollect object.

@details The DataCollect task interacts with a motor encoder and UserInterface task object
to produce incremental data sets of encoder position and time. The file name is labeled
as 'main.py' in order to be recognized by the Nucleo and constantly ran. The DataCollect task
stores the encoder position and time in an array and interacts with the UserInterface task 
object via UART. The encoder recording has a default sample rate of 5Hz and a duration of 10 seconds.

@author Grant Gallagher

@date November 3, 2020
'''

import utime
import array
import pyb
from pyb import UART

class DataCollect:
    '''
    @brief      A remote FSM task that is used to collect the position of of an encoder over time.
    
    @details    When continuously ran on the Nucleo, this FSM interacts with a front-end user interface
                to facilitate control of an encoder driver. The user is capable of starting the
                recording the encoder position, stopping the recording, and printing a plot/.csv file of
                the results. This task facilitates the collection process.
    '''
    ## State 0: Initialization
    S0_INIT = 0
    
    ## State 1: Wait for user input.
    S1_WAIT = 1
    
    ## State 3: Collect encoder data.
    S2_COLLECT_DATA = 2
       
    
    def __init__(self, interval, duration):
        '''
        @brief Constructs a Datacollect object.
        @param interval The time, in milliseconds, between runs of the task
        @duration The maximum time, im milliseconds, for how long the encoder position is recorded
        '''
        ##  The amount of time in milliseconds between runs of the task.
        self.interval = interval
        
         ##  The amount of time to run the task unless terminated by the user.
        self.duration = duration * 1000
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration.
        self.start_time = utime.ticks_ms()
                
        ## The "timestamp" for when the task should run next.
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The UART console.
        self.uart = UART(2)
        
        ## The character value of the user input.
        self.user_input = 0
        
        ## The second most recent recorded encoder position.
        self.position_old = 0
        
        ## The most recent recorded encoder position.
        self.position_new = 0
        
        ## The count difference between position_new and position_old.
        self.delta = 0
        
        ## The timer object used for encoder readings.
        self.tim = pyb.Timer(3)
        
        ## An array containing encoder position since recording.
        self.array_position = array.array("i")
        
        ## An array containing time since recording.
        self.array_time = array.array("i")
        
        ## The state of the data collection task.
        self.is_collecting = False
        
        ## The time of when data collection begins.
        self.collecting_time = 0

    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## The timestamp for the current task run.
        self.current_time = utime.ticks_ms()
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.current_time,self.next_time) >= 0):
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.tim.init(prescaler = 0, period = 0xFFFF)                               # Initialization of timer
                self.tim.channel(1, pin = pyb.Pin(pyb.Pin.cpu.A6), mode = pyb.Timer.ENC_AB) # Timer channel 1 initialized to pin 'A6'
                self.tim.channel(2, pin = pyb.Pin(pyb.Pin.cpu.A7), mode = pyb.Timer.ENC_AB) # Timer channel 2 initialized to pin 'A7'
                self.transitionTo(self.S1_WAIT)                                             # Transition to State 1   
                
            elif(self.state == self.S1_WAIT):
                # Run State 1 Code
                if self.uart.any() != 0:                # Check for user input
                    self.user_input = self.uart.readchar() # Store user input
                    self.check_input()
                
            elif(self.state == self.S2_COLLECT_DATA):
                # Run State 3 Code
                self.update() # Update the current position
                self.array_position.append(self.get_position())
                self.array_time.append(utime.ticks_diff(self.current_time,self.collecting_time)) 
                
                if self.uart.any() != 0:                   # Check for user input
                    self.user_input = self.uart.readchar() # Store user input
                    self.check_input()
                elif (utime.ticks_diff(self.current_time,self.collecting_time)) > self.duration: # Checks if time has exceeded duration
                    self.uart.write("p: " + str(self.array_position) + " t: " + str(self.array_time)) # Output data
                    self.transitionTo(self.S1_WAIT)
                                        
            else:
                # Error handling
                pass
                
            self.next_time = utime.ticks_add(self.current_time,self.interval) # Update the "Scheduled" timestamp 
                
    def transitionTo(self, newState):
        '''
        @brief Updates the current state variable.
        @param newState The state in the FSM to transition to.
        '''
        self.state = newState
    
    
    def check_input(self):
        '''
        @brief      Checks the input character of the user using ascii characters.
        '''
        if (self.user_input == 103 or self.user_input == 71): # ascii: 103 == 'g' | 71 == 'G'
            if self.is_collecting == False:
                self.uart.write("Encoder position is now being recorded for a maximum of 10s. Press 'S' at any time.") # Prompt UI current state
                self.array_time = array.array("i")         # Initialize new time array
                self.array_position = array.array("i")     # Initialize new position array
                self.collecting_time = utime.ticks_ms()    # Initialize starting time of data collection
                self.set_position(0)                       # Zero the position of the encoder.
                self.is_collecting = True                  # Begin collecting data
                self.transitionTo(self.S2_COLLECT_DATA)    # Transition to State 3
            else:
                self.uart.write("Recording in-progress.") # Prompt UI current state
                self.transitionTo(self.S2_COLLECT_DATA)    # Transition to State 3
                        
        elif (self.user_input == 115 or self.user_input == 83): # ascii: 115 == 's' | 83 == 'S'
            if self.is_collecting == True:
                self.uart.write("p: " + str(self.array_position) + " t: " + str(self.array_time)) # Output data
                self.is_collecting = False                       # Stop collecting data
                self.transitionTo(self.S1_WAIT)    # Transition to State 1
            else:
                self.uart.write("There is no recording in-progress.") # Prompt UI current state
                self.transitionTo(self.S1_WAIT)           # Transition to State 1
                        
        else:
            self.uart.write("Invalid command.")       # Prompt UI response
            
            
    def update(self):
        '''
        @brief  Corrects under/overflow for the encounter count and advances
                delta, most recent position, and second most recent position.
        '''
        self.delta = self.tim.counter() - self.position_new # Difference between last and current position
        
        if self.delta > 0xFFFF/2: # Check for underflow
            self.delta -= 0xFFFF  # Fixes underflow (for 16-bit encoder)
            
        elif self.delta < -0xFFFF/2: # Check for overflow
            self.delta += 0xFFFF     # Fixes overflow (for 16-bit encoder)
            
        else:
            # Error handling
            pass
        
        self.position_old = self.position_new               # Advance second most recent encoder position
        self.position_new  = self.position_old + self.delta # Advance most recent coder position
            
    def get_position(self):
        '''
        @brief  Returns the absolute value of the encoder position since initialization
                or zero-ing -- accounting for over/underflow.
        @return The most recent value of the encoder position.
        '''
        return self.position_new
    
    def set_position(self, setValue):  
        '''
        @brief          Sets all forms of the current encoder position to a specified value.
        @param setValue An integer value for the encoder position (count) to be set to.
                        By default, setValue is 0 in order to zero the encoder.
        ''' 
        self.position_old = setValue
        self.position_new = setValue
        self.tim.counter(setValue)
            
    def get_delta(self):
        '''
        @brief      Updates and returns the difference in encoder position.
        @return     The difference between position_new and position_old.
        '''
        self.delta = self.position_new - self.position_old
        return self.delta


## A DataCollect task object with a sample frequency of 5Hz and duration of 10s.
task_1 = DataCollect(200, 10) # Construct a DataCollect object

while True: # Run forever on Nucleo
    task_1.run()