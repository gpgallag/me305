'''
@file blinky.py

This file implements multitasking and MicroPython on physical hardware to simultaneously
produce outputs to a physical and virtual LED.

This file implements two finite-state machines to be ran on a Nucleo STM-L476RG development board.
The first FSM outputs a sinusoidal/sawtooth signal to a physical led on the hardware.
The second FSM outputs a square wave signal to the virtual LED - by printing "LED ON/OFF" to the console. 

@package Lab0x02
@brief This package contains blinky.py

@details This package is used to control a virtual and physical led on the Nucleo L476RG.

@author Grant Gallagher

@date October 13, 2020
'''

import math
import pyb

class virtualLED:
    '''
    @brief      A finite state machine to control a virtual LED.
    @details    This class implements a finite state machine to control the waveform of a
                virtual LED on the Nucleo STM-L476RG development board.
                The LED blinks on and off by printing to the console. The period 
                of the blinks can be altered.
                
                This class implements a finite state machine to control the waveform of a
                physical LED on a Nucleo STM-L476RG development board. The period, and switching
                interval can be altered, and the shape of the output can be either sinusoidal or sawtooth.
    '''
    ## Constant defining State 0: Initialization.
    S0_INIT = 0
    
    ## Constant defining State 1: The virtual LED is off.
    S1_OFF  = 1
    
    ## Constant defining State 4: The virtual LED is on.
    S2_ON  = 2

    def __init__(self, period):
        '''
        @brief         Creates a virtualLED object.
        @param period  The period of blinking for the LED signal. (milliseconds)
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the period parameter.
        self.period = period
         
        ## The timestamp for the first iteration.
        self.start = pyb.millis()
    
        ## The interval of time, in seconds, between runs of the task.
        #  By default set to 1/10th of the period.
        self.interval = period/10
    
        ## The "timestamp" for when to run the task next.
        self.next_time = self.interval + pyb.elapsed_millis(self.start)
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task.
        @details    The task can be one of three various states: initialization, virtual LED off,
                    or virtual LED on.
        '''   
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if (pyb.elapsed_millis(self.start) >= self.next_time):
            
            if(self.state == self.S0_INIT): # Checks the desired waveform
                # Run State 0 Code
                self.transitionTo(self.S1_OFF)
                print('Virtual LED set blink with a period of ' + str(self.period/1000) + ' seconds.')

            # Run State 1 Code      
            elif(self.state == self.S1_OFF):      
                # The virtual LED is currently off.
                if (pyb.elapsed_millis(self.start)) % self.period >= self.period/2: 
                    self.transitionTo(self.S2_ON)  # State = 2
                    print("LED ON")
                    
            # Run State 2 Code          
            elif(self.state == self.S2_ON):
                # The virtual LED is currently on.
                if (pyb.elapsed_millis(self.start)) % self.period <= self.period/2:
                    self.transitionTo(self.S1_OFF) # State = 1
                    print("LED OFF")
              
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.next_time += self.interval # Update the "Scheduled" timestamp
            
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run.
        '''
        self.state = newState  
        
        
class physicalLED:
    '''
    @brief      A finite state machine to control a physical LED.
    @details    This class implements a finite state machine to control the waveform of a
                physical LED on a Nucleo STM-L476RG development board. The period, and switching
                interval can be altered, and the shape of the output can be either sinusoidal or sawtooth.
    '''
    ## Constant defining State 0: Initialization.
    S0_INIT = 0
    
    ## Constant defining State 1: The physical LED has a sinusoidal waveform.
    S1_SIN  = 1
    
    ## Constant defining State 2: The physical LED has a sawtooth waveform.
    S2_SAW  = 2     

    def __init__(self, LED, period, shape, duration):
        '''
        @brief          Creates a physicalLED object.
        @param LED      The LED object (channel) which is to be controlled.
        @param period   The period of the waveform for the LED signal. (milliseconds)
        @param shape    The starting shape of the waveform signal: 'SIN' or 'SAW'.
        @param duration The time between switching from one shape to another.
        '''  
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the period parameter.
        self.period = period
        
        ## A class attribute "copy" of the shape parameter.
        self.shape = shape
         
        ## The timestamp for the first iteration.
        self.start = pyb.millis()
    
        ## The interval of time, in seconds, between runs of the task.
        #  By default set to 1/100th of the period.
        self.interval = period/100
    
        ## The "timestamp" for when to run the task next.
        self.next_time = self.interval + pyb.elapsed_millis(self.start)
        
        ## The duration between when the waveform pattern switches.
        self.duration = duration
        
        ## The "timestamp" for when to switch between physical LED patterns.
        self.switch_time = self.duration + pyb.elapsed_millis(self.start)

        ## A Channel object created on tim2 , set up to output PWM on pina5
        self.t2ch1 = LED
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task.
        @details    The task can be one of three various states: initialization,
                    physical LED with a sinusoidal waveform, or physical LED
                    with a sawtooth waveform. The period of each waveform remains the same.
        '''  
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if (pyb.elapsed_millis(self.start) >= self.next_time):
            
            if(self.state == self.S0_INIT): # Checks the desired waveform
                # Run State 0             
                if self.shape == 'SIN': # Initial shape is sinusoidal
                    self.transitionTo(self.S1_SIN)
                elif self.shape == 'SAW': # Initial shape is sawtooth
                    self.transitionTo(self.S2_SAW)
                else: # Invalid waveform parameter. By default set to sinusoidal
                    self.transitionTo(self.S1_SIN)    
                    print('Invalid waveform. Initializing to SIN shape.')
                    
                print('Physical LED set to ' + self.shape + ' with a period of ' + str(self.period/1000) + ' seconds.')
                print('The physical LED will switch shapes every ' + str(self.duration/1000) + ' seconds.')    
                
                   
            # Run State 1 Code         
            elif(self.state == self.S1_SIN):
                self.t2ch1.pulse_width_percent(50 * (math.sin(2*math.pi*(pyb.elapsed_millis(self.start))/self.period) + 1)) # Output sinusoidal signal to physical LED
                if (pyb.elapsed_millis(self.start) >= self.switch_time):
                    print("Physical LED switching to SAW")
                    self.switch_time += self.duration # Updates interval for when to switch between patterns
                    self.transitionTo(self.S2_SAW) # State = 4
                
            # Run State 2 Code         
            elif(self.state == self.S2_SAW): 
                self.t2ch1.pulse_width_percent(100*(((pyb.elapsed_millis(self.start))%self.period)/self.period)) # Output sawtooth signal to physical LED
                if (pyb.elapsed_millis(self.start) >= self.switch_time):
                    print("Physical LED switching to SIN")
                    self.switch_time += self.duration # Updates interval for when to switch between patterns
                    self.transitionTo(self.S1_SIN) # State = 3
            
            
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.next_time += self.interval # Update the "Scheduled" timestamp
            
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run.
        '''
        self.state = newState


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Test and debugging code
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


## The pin header that is assigned to the physical LED on the Nucleo
pinA5 = pyb.Pin(pyb.Pin.cpu.A5)

## A Timer object using timer 2 on the Nucleo
tim2  = pyb.Timer(2, freq = 20000)

## A Channel object created on tim2 , set up to output PWM on pina5
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

if __name__ == '__main__':
    ## A virtualLED object set to blink with a period of 2 seconds.
    task2_Virtual = virtualLED(2000)
    
    ## A physicalLED object set to a sinusoidal shape with a period of 10 seconds - alternating between sawtooth every 30 seconds.
    task1_Physical = physicalLED(t2ch1, 10000, 'SIN', 30000)
    
    while True: # Runs in a constant loop for hardware.
        task1_Physical.run()
        task2_Virtual.run()