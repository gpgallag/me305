'''
@file main.py
'''
import pyb
from encoderDriver import EncoderDriver
from motorDriver import MotorDriver
from InterfaceRemote import InterfaceRemote
from taskControl import TaskControl
if __name__ == '__main__':
    
    ## Init Motor
    pin_nSleep = pyb.Pin(pyb.Pin.cpu.A15)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    tim3 = pyb.Timer(3, freq = 20000)
    moe1 = MotorDriver(pin_nSleep, pin_IN1, pin_IN2, tim3, debug = False)
    
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)
    tim3 = pyb.Timer(3, freq = 20000)
    moe2 = MotorDriver(pin_nSleep, pin_IN1, pin_IN2, tim3, debug = False)
    
    # Init Encoder
    Apin1 = pyb.Pin(pyb.Pin.cpu.B6)                 # Construct the encoder pin object (1/2)
    Bpin1 = pyb.Pin(pyb.Pin.cpu.B7)                 # Construct the encoder pin object (2/2)
    tim4 = pyb.Timer(4, prescaler=0, period=0xFFFF) # Construct the timer object
    enc1 = EncoderDriver(Apin1, Bpin1, tim4, debug = False) # Construct an EncoderDriver object
    
    Apin2 = pyb.Pin(pyb.Pin.cpu.C6)                 # Construct the encoder pin object (1/2)
    Bpin2 = pyb.Pin(pyb.Pin.cpu.C7)                 # Construct the encoder pin object (2/2)
    tim8 = pyb.Timer(8, prescaler=0, period=0xFFFF) # Construct the timer object
    enc2 = EncoderDriver(Apin2, Bpin2, tim8, debug = False) # Construct an EncoderDriver object

    # Init Control
    interval = 1000
    
    duration = 1000
    
    task = TaskControl(enc1, moe1, 20000, 1000000)
    
    task2 = InterfaceRemote(50)

    while True:
        task.run()
        task2.run()

        
