'''
@file  taskControl.py

@brief  This file contains a finite-state machine task that repetitvely iterates closed-loop control of a motor.

@details  This task receives a commands from an InterfaceRemote object, via a shares file,
which sets the Ki, Kp, Kd, and motor reference speeds for a motor step response. The task
also receives a "start" signal from the InterfaceRemote task to begin stepping the motor.

@package Lab0x06

@author Grant Gallagher

@date November 24, 2020
'''
import utime
import shares
from closedLoop import ClosedLoop

class TaskControl:
    '''
    @brief      Encapsulates the operation of closed-loop PID control of a motor.
    
    @details    This class initializes a ClosedLoop controller object that is used
                to correct the speed and response of a motor. The class also
                contains a finite-state machine that is used to interact with
                the back-end of a UI and step a motor. An update task is created
                to run periodically so that the controller can continiously correct
                for the real motor behavior.
    '''
    ## State 0: Initialization
    S0_INIT = 0
    
    ## State 1: Wait for user input.
    S1_WAIT = 1
    
    ## State 2: Collect encoder data.
    S2_COLLECT_DATA = 2
    
    def __init__(self, encoder, motor, interval, duration):
        '''
        @brief Constructs a TaskControl object.
        @param encoder  An EncoderDriver object.
        @param motor    A MotorDriver object
        @param interval The number of microseconds between desired runs of the task.
        @param duration The total amount of time (microseconds) to run the motor step.
        '''
        ## An encoder object.
        self.encoder = encoder
        
        ## An motor object.
        self.motor = motor 
        
        ##  The amount of time in microseconds between runs of the task. 
        self.interval = interval
        
        ## The measured speed of the motor. 
        self.W_meas = 0
        
        ## The level (%/rpm) sent to the motor driver.
        self.level = 0
        
        ## The ratio between the motor and encoder pully (1:4)
        self.gear_ratio = 4
             
        ##  The amount of time to run the task unless terminated by the user.
        self.duration = duration
    
        ##  The timestamp for when the task begins collecting motor data.
        self.collecting_start_time = 0
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration.
        self.start_time = utime.ticks_us()
        
        # The gear ratio between the encoder/motor pulleys
        self.gear_ratio = 4
        
        ## The timestamp for the current task run.
        self.current_time = 0
        
        ## The "timestamp" for when the task should run next.
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ##  A ClosedLoop object that sets the parameters for closed-loop feedback control.
        self.controller = ClosedLoop(shares.Kp, shares.Ki, shares.Kd, self.interval, debug = False)
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## The timestamp for the current task run.
        self.current_time = utime.ticks_us()
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.current_time,self.next_time) >= 0):
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.motor.enable()
                self.encoder.set_position(0)
                self.transitionTo(self.S1_WAIT) # Transition to State 1   
            
            elif(self.state == self.S1_WAIT):
                # Run State 1 Code
                if shares.collect == True:
                    self.controller.set_Kp(shares.Kp) # Set the Kp gain to the value in shares
                    self.controller.set_Ki(shares.Ki) # Set the Ki gain to the value in shares
                    self.controller.set_Kd(shares.Kd) # Set the Kd gain to the value in shares
                    self.collecting_start_time = utime.ticks_us() # Create a timestamp for when the task is started.
                    self.transitionTo(self.S2_COLLECT_DATA) # Transition to State 2  
                    
            elif(self.state == self.S2_COLLECT_DATA):
                # Run State 2 Code
                self.update()
                
                if (utime.ticks_diff(self.current_time, self.collecting_start_time) >= self.duration):
                    self.motor.set_duty(0) # Turn the motor off
                    shares.collect = False # Reset the shares collect variable
                    self.transitionTo(self.S1_WAIT) # Transition to State 1   

            else:
                # Invalid state code (error handling)
                pass
        
            self.next_time = utime.ticks_add(self.current_time,self.interval) # Update the "Scheduled" timestamp
                
    def transitionTo(self, newState):
        '''
        @brief Updates the current state variable.
        @param newState The state in the FSM to transition to.
        '''
        self.state = newState
    
    def update(self):
        '''
        @brief Runs one iteration of closed-loop control to set the motor duty.
        @details Updates the encoder to measure the angular velocity (rpm). Then, calculates
        the error between desired motor speed and actual motor speeds. Calculates necessary
        level to set the motor speed from ClosedLoop.update().
        '''
        self.encoder.update() # Updates encoder position

        self.W_meas = self.encoder.get_delta() * 10**6 * 60 / (self.interval * 4000) # Calculate motor speed (rpm)
        
        shares.array_time.append(utime.ticks_diff(self.current_time, self.collecting_start_time)) # Update time array
        shares.array_W_meas.append(int(self.W_meas))                                              # Update speed array
        
        self.level = int(self.controller.update(shares.W_ref, self.W_meas)) # Calculates level
        
        self.motor.set_duty(self.level) # Sets motor speed to level
        
        

if __name__ == '__main__':
    import pyb
    from motorDriver import MotorDriver
    from encoderDriver import EncoderDriver
    # from closedLoop import ClosedLoop
    print("WTF")
    # The following section is used to debug the TaskControl task. The motor
    # is set to run at a desired speed based on closed loop control. Any code within the
    # if __name__ == '__main__' block will only run when the script is executed as
    # a standalone program. If the script is imported as a module the code block will not run.
    ## Init Motor
    pin_nSleep = pyb.Pin(pyb.Pin.cpu.A15)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    tim3 = pyb.Timer(3, freq = 20000)
    moe1 = MotorDriver(pin_nSleep, pin_IN1, pin_IN2, tim3, debug = False)
    
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)
    tim3 = pyb.Timer(3, freq = 20000)
    moe2 = MotorDriver(pin_nSleep, pin_IN1, pin_IN2, tim3, debug = False)
    
    # Init Encoder
    Apin1 = pyb.Pin(pyb.Pin.cpu.B6)                 # Construct the encoder pin object (1/2)
    Bpin1 = pyb.Pin(pyb.Pin.cpu.B7)                 # Construct the encoder pin object (2/2)
    tim4 = pyb.Timer(4, prescaler=0, period=0xFFFF) # Construct the timer object
    enc1 = EncoderDriver(Apin1, Bpin1, tim4, debug = False) # Construct an EncoderDriver object
    
    Apin2 = pyb.Pin(pyb.Pin.cpu.C6)                 # Construct the encoder pin object (1/2)
    Bpin2 = pyb.Pin(pyb.Pin.cpu.C7)                 # Construct the encoder pin object (2/2)
    tim8 = pyb.Timer(8, prescaler=0, period=0xFFFF) # Construct the timer object
    enc2 = EncoderDriver(Apin2, Bpin2, tim8, debug = False) # Construct an EncoderDriver object

    # Init Task
    interval = 20
    # # task = TaskControl(enc, moe, interval, 100000)
    # controller = ClosedLoop(0.1, 0.025, 0.001, debug = True)
    moe1.enable()
    
    start_time = pyb.millis()
    next_time = interval + pyb.elapsed_millis(start_time) 
    moe1.set_duty(-50)
    count = 0
    W_meas = 0
    level = 0
    print("HELLO")
    while True:
        if (pyb.elapsed_millis(start_time) >= next_time):
            # task.update()
            enc1.update()
            W_meas = enc1.get_delta() * 4 * 10**3 * 60 / (interval * 1000 * 4) # Calculate motor speed (rpm)                                     # Update speed array
            print("W_meas = " + str(W_meas))
            
            
            count += 1
            next_time += interval # Update the "Scheduled" timestamp