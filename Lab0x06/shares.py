'''
@file shares.py

@brief A container for all the inter-task variables.

@package Lab0x06
'''
import array

## The shared command character sent from Front
cmd = None

## The shared response character sent from Remote
resp = None

## The shared state of the ClosedLoop task that represents if it is active.
collect = False

## The shared motor speed reference value.
W_ref = 700

## The shared proportional gain value.
Kp = 0

## The shared integral gain value.
Ki = 0

## The shared derivative gain value.
Kd = 0

## A shared array containing encoder motor speed since recording.
array_W_meas = array.array("i")
        
## A shared array containing time since recording.
array_time = array.array("i")


