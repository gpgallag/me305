'''
@file interfaceFront.py

@brief This file contains a finite-state machine that runs the front-end of a user interface.

@details The InterfaceFront task behaves as a UI that runs locally on a PC. The interface
interacts with a back-end userface, InterfaceRemote, that is stored remotely on the Nucleo
L476RG. Within this UI, the user can specify the P, I, and D gains for a motor controller,
set the reference velocity, step the motor, and export the respective data/plot.

@package Lab0x06

@brief This package contains interfaceFront.py, interfaceRemote.py, taskControl.py,
       closedLoop.py, motorDriver.py, encoderDriver.py, and shares.py.

@author Grant Gallagher

@date November 24, 2020
'''

import time
import serial
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import csv
import array

class InterfaceFront:
    '''
    @brief   This class encapsulates the front-end of a user interface used to control a motor.
    
    @details This class contains a finite-state machine that is used to interact
             between the front-end of the UI (ran on a PC) and a controller task.
             The front-end UI is capable of sending commands to change/set the PID gains
             of the controller, step the motor, and export a graph/.csv file containing the
             time and speed of the motor.
    '''
    ## State 0: Initialization
    S0_INIT          = 0
    
    ## State 1: Wait for user input.
    S1_WAIT_FOR_CMD  = 1
    
    ## State 2: Waiting for response from the back-end of the UI.
    S2_WAIT_FOR_RESP = 2

    def __init__(self, interval):
        '''
        @brief Constructs a InterfaceFront task object.
        @param interval A number of seconds between desired runs of the task.
        '''
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
                
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval

        ## The Serial Port object.
        self.serial_port = serial.Serial('COM3', baudrate = 115200 ,timeout = 1)
        self.serial_port.close()
        
        ## The command variable which stores the user's input.
        self.cmd = ''
        
        ## An array containing the motor velocity since recording.
        self.array_position = array.array("f")
        
        ## An array containing the timestamps since recording.
        self.array_time = array.array("f")
        
        ## The current state of the data collection task.
        self.task_finished = False
        
        ## The conversion factor for sorting the .csv output.
        self.conversion_factor = 1
        
        ## The response variable which prompts the user with feedback.
        self.response = ''
               
        ## The motor reference speed.
        
        self.W_ref = 700
        
        ## The controller proportional gain.
        self.Kp = 0
        
        ## The controller integral gain.
        self.Ki = 0
        
        ## The controller derivative gain.
        self.Kd = 0
        

    def run(self):
        '''
        @brief Runs one iteration of the task.
        '''
        ## The timestamp for the current task run.
        self.current_time = time.time()
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if self.current_time - self.next_time >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.response = ('Welcome. Please reset the Nucleo with the black button before continuing.\n'
                                 '          Commands and current parameters are listed above.')
                self.transitionTo(self.S1_WAIT_FOR_CMD) # Transition to State 1
        
        
            elif(self.state == self.S1_WAIT_FOR_CMD):
                # Run State 1 Code
                self.clear()          # Clear the serial port
                self.print_commands() # Prompt the text-based UI
                print('\nResponse: '+self.response+'') # Display user feedback
                time.sleep(0.2)
                self.cmd = input("Command: ") # Prompt user input
                
                if self.cmd == '': # Check for non-empty input
                    self.response = ('Error. Command was empty.')
                    
                elif self.cmd == 'p' or self.cmd == 'P': # Checks for P command
                    self.cmd = input('Set the Kp value to: ')
                    try: # Checks if input is numeric
                        float(self.cmd)  
                        if float(self.cmd) >= 0: # Checks if input is positive
                            self.serial_port.open() # Opens Nucleo serial
                            self.serial_port.write(str('Kp'+self.cmd).encode('ascii')) # Sends user input to Nucleo serial
                            self.response = ('The proportional gain has been set to '+str(self.cmd))
                            self.transitionTo(self.S2_WAIT_FOR_RESP) # Transition to State 2
                        else:
                            raise ValueError
                    except ValueError:
                        self.response = ("'"+str(self.cmd)+"' is not a valid gain. Please input a positive number.")
                        
                elif self.cmd == 'i' or self.cmd == 'I': # Checks for I command
                    self.cmd = input('Set the Ki value to: ')
                    try: # Checks if input is numeric
                        float(self.cmd)
                        if float(self.cmd) >= 0: # Checks if input is positive
                            self.serial_port.open() # Opens Nucleo serial
                            self.serial_port.write(str('Ki'+self.cmd).encode('ascii')) # Sends user input to Nucleo serial
                            self.response = ('The integral gain has been set to '+str(self.cmd))
                            self.transitionTo(self.S2_WAIT_FOR_RESP) # Transition to State 2
                        else:
                            raise ValueError
                    except ValueError:
                        self.response = ("'"+str(self.cmd)+"' is not a valid gain. Please input a positive number.")
                        
                elif self.cmd == 'd' or self.cmd == 'D': # Checks for D command
                    self.cmd = input('Set the Kd value to: ')
                    try: # Checks if input is numeric
                        float(self.cmd)
                        if float(self.cmd) >= 0: # Checks if input is positive
                            self.serial_port.open() # Opens Nucleo serial
                            self.serial_port.write(str('Kd'+self.cmd).encode('ascii')) # Sends user input to Nucleo serial
                            self.response = ("The derivative gain has been set to "+str(self.cmd))
                            self.transitionTo(self.S2_WAIT_FOR_RESP) # Transition to State 2
                        else:
                            raise ValueError
                    except ValueError:
                        self.response = ("'"+str(self.cmd)+"' is not a valid gain. Please input a positive number.")
                        
                elif self.cmd == 'w' or self.cmd == 'W': # Checks for motor speed command
                    self.cmd = input('Set the motor reference speed (rpm) to:  ')
                    try: # Checks if input is numeric
                        float(self.cmd)
                        self.serial_port.open() # Opens Nucleo serial
                        self.serial_port.write(str('W'+self.cmd).encode('ascii')) # Sends user input to Nucleo serial
                        self.response = ('The motor reference speed has been set to '+str(self.cmd)+'rpm')
                        self.transitionTo(self.S2_WAIT_FOR_RESP) # Transition to State 2
                    except ValueError:
                        self.response = ("'"+str(self.cmd)+"' is not a valid motor speed. Please input a number.")
                        
                elif self.cmd == 's' or self.cmd == 'S':     # Checks for start command
                    self.serial_port.open()                  # Opens Nucleo serial
                    self.serial_port.write('S'.encode('ascii'))
                    self.response = ('The step response test has been initiated.')
                    self.transitionTo(self.S2_WAIT_FOR_RESP) # Transition to State 2
                    
                elif self.cmd == 'e' or self.cmd == 'E':     # Checks for export command
                    self.serial_port.open()                  # Opens Nucleo serial
                    self.serial_port.write('E'.encode('ascii'))
                    self.transitionTo(self.S2_WAIT_FOR_RESP) # Transition to State 2
                
                else:
                    print("Error. Command is not recognized")
                    pass
                
                print("Processing...")
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                ## Output from Nucleo serial.
                self.output = str(self.serial_port.readline().decode('ascii'))
                self.serial_port.close() # Close Nucleo serial
                if self.output != '': # Checks for non-empty response
                    if self.output[0] == 'p': # 'p' is the beginning of the array
                        self.convert_to_array(self.output)
                        self.clear()        # Clear UI
                        self.export_plot()  # Export plot
                        self.export_data()  # Export .csv
                        self.task_finished = True # Close the program
                        
                    elif self.output[0:2] == 'Kp': # Kp gain response
                        self.Kp = self.output[2:]  # Assign remaining characters
                        self.serial_port.close()   # Close Nucleo serial
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                        
                    elif self.output[0:2] == 'Ki': # Ki gain response
                        self.Ki = self.output[2:]  # Assign remaining characters
                        self.serial_port.close()   # Close Nucleo serial
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                                
                    elif self.output[0:2] == 'Kd': # Kd gain response
                        self.Kd = self.output[2:]  # Assign remaining characters
                        self.serial_port.close()   # Close Nucleo serial
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                        
                    elif self.output[0] == 'W':       # Motor speed response
                        self.W_ref = self.output[1:]  # Assign remaining characters
                        self.serial_port.close()      # Close Nucleo serial
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                    
                    elif self.output == 'E': # Error response
                        self.response = ('Error. The remote UI does not know how to respond')
                    else:
                        pass
                    
                else:
                    pass
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                        
            self.next_time = self.next_time + self.interval # Update the "Scheduled" timestamp 

    def transitionTo(self, newState):
        '''
        @brief Updates the current state variable.
        @param newState The state in the FSM to transition to.
        '''
        self.state = newState
        
    def convert_to_array(self, string):
        '''
        @brief Converts the compressed array sent from the back-end to individual arrays.
        @param string The compressed array of motor speeds and timestamps.
        '''
        ## Stores the numerical value of the UART output as an integer.
        self.string_number = 0
        ## A placeholder to determine if the UART output value is negative.
        self.is_negative = 1
        ## A reference for the position and time arrays coming from the UART.
        self.ref_array= self.array_position
        for index in range(0, len(string)-1): # Parses the string sent by the UART
            if string[index] == "-":          # Checks for a negative number
                self.is_negative = -1         # Assigns placeholder value
            elif string[index] == "t":        # Checks for the beginning of the time array
                self.ref_array= self.array_time
                self.conversion_factor = 0.001 # Changes the conversion factor from position to time (ms to s)
            elif str(string[index]).isnumeric(): # Checks for a numerical value
                self.string_number = self.string_number * 10 + int(string[index])
            if string[index] == "," or string[index] == "]":
                self.ref_array.append(self.string_number * self.is_negative * self.conversion_factor)
                self.string_number = 0 # Resets the value for next iteration
                self.is_negative = 1   # Resets the value for next iteration    
        self.array_position.remove(0)
        self.array_time.remove(0)
        
    def export_plot(self):
        '''
        @brief Creates and exports a plot for the motor speed versus time as a .png file.
        @details Labels the y-axis (speed), x-axis (time), and title of the plot, and exports it as a "motor_plot.png".
        '''
        plt.plot(self.array_time, self.array_position) # Plots the encoder position and time
        plt.xlabel('Time, t [ms]')                      # Label x-axis
        plt.ylabel('Speed, Ω [rpm]')            # Label y-axis
        plt.title('Motor Speed Step Response')      # Label plot title
        plt.savefig('motor_plot.png')                # Save the plot under file
        
        Kp_legend = mpatches.Patch(color = 'white', label = ('Kp = ' + str(self.Kp)))
        Ki_legend = mpatches.Patch(color = 'white', label = ('Ki = ' + str(self.Ki)))
        Kd_legend = mpatches.Patch(color = 'white', label = ('Kd = ' + str(self.Kd)))
        plt.legend(handles = [Kp_legend, Ki_legend, Kd_legend], loc = 'lower right')


        print('The step response plot has been exported as: "motor_plot.png"')
        
    def export_data(self):
        '''
        @brief Exports the encoder raw data as a .csv file to the current folder.
        @details The raw data is exported as "motor_data.csv".
        '''
        with open('motor_data.csv', mode = 'w') as encoder_file: # Open a .csv file in write mode
            ## A writer object responsible for converting the user’s data into delimited strings
            self.value_writer = csv.writer(encoder_file, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_MINIMAL) # Formats the .csv file
            for data_set in range(len(self.array_position)-1):                                           # Parses the position/time arrays for data
                self.value_writer.writerow( [self.array_position[data_set], self.array_time[data_set]] ) # Writes the position/time for the nth data set
        print('The raw motor data has been exported as: "motor_data.csv"')
        
    def clear(self):
        '''
        @brief      Flushes the serial port of any chararacters.
        '''
        print("\x1B\x5B2J")
        print("\x1B\x5BH")
        
    def print_commands(self):
        '''
        @brief      Prints the list of available commands to comminicate with the UI back-end on the Nucleo.
        '''
        print("                      Console Commands:")
        print("-------------------------------------------------------------------")
        print("P - Set proportial gain           S - Start motor step response")
        print("I - Set integral gain             E - Export data and plot")
        print("D - Set derivative gain")
        print("W - Set motor ref. speed (rpm)")
        print("-------------------------------------------------------------------\n")
        print('Current parameters:')
        print('    Kp = '+str(self.Kp))
        print('    Ki = '+str(self.Ki))
        print('    Kd = '+str(self.Kd))
        print('    W  = '+str(self.W_ref)+'rpm')
        
            
        
if __name__ == '__main__':
    ## A UserInterface task object with a timing interval of 0.1s
    UI_task = InterfaceFront(.1) # Construct a UserInterface object

    while UI_task.task_finished == False:
        UI_task.run()
