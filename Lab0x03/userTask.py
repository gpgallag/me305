'''
@file userTask.py

@brief This file contains a UART user interface to interact with an encoder driver.

@details The user interface operates asynchronously with the running of the motor driver 
to prompt several commands to the user and wait for a response. The UI can zero the encoder, 
get the encoder position, and get the encoder delta.

@author Grant Gallagher

@date October 20, 2020
'''
import shares
import pyb
from pyb import UART

class UserTask:
    '''
    @brief      An inter-task communication interface using UART.
    @details    This class runs asynchronously with the Encoder task to provide
                non-blocking interaction with the Encoder. Checks the REPL input
                for commands to: Zero/Print the encoder position, print the encoder
                delta, and re-print the commands.
    '''
    ## State 0: Initialization.
    S0_INIT = 0
    
    ## State 1: Wait for user command.
    S1_WAIT_CMD = 1
    
    ## State 2: Zero the encoder position.
    S2_SET_POSITION = 2
    
    ## State 3: Get the encoder position.
    S3_GET_POSITION = 3
    
    ## State 4: Get the encoder delta.
    S4_GET_DELTA = 4
    
    ## State 5: Print the encoder task commands.
    S5_PRINT_COMMANDS = 5
    

    def __init__(self, interval):
        '''
        @brief      Constructs a UserTask object using UART.
        '''
        ##  The amount of time in milliseconds between runs of the task
        self.interval = interval
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start = pyb.millis()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.interval + pyb.elapsed_millis(self.start)

        ## Serial port
        self.uart = UART(2)
        
        
    def print_commands(self):
        '''
        @brief      Prints the list of available commands to comminicate with the encoder.
        '''
        print("Interact with the encoder using the following commands:")
        print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n")
        print("        Z - Zero the encoder position")
        print("        P - Print out the encoder position")
        print("        D - Print out the encoder delta \n")
        print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n")


    def clear(self):
        '''
        @brief      Flushes the REPL.
        '''
        print("\x1B\x5B2J")
        print("\x1B\x5BH")


    def run(self):
        '''
        @brief          Initializes and runs the UART.
        '''
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if (pyb.elapsed_millis(self.start) >= self.next_time):
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.print_commands() # Prompt the user with acceptable commands
                self.transitionTo(self.S1_WAIT_CMD)
                
            elif(self.state == self.S1_WAIT_CMD):
                # Run State 1 Code
                if self.uart.any():                                   # Checks for user input
                    user_input = self.uart.read()
                    
                    if (user_input.decode('utf-8') == ('z' or 'Z')):  # Command to zero the encoder position.
                        self.transitionTo(self.S2_SET_POSITION)
                        
                    elif (user_input.decode('utf-8') == ('p' or 'P')): # Command to print out the encoder position.
                        self.transitionTo(self.S3_GET_POSITION)
                        
                    elif (user_input.decode('utf-8') == ('d' or 'D')): # Command to print out the encoder delta.
                        self.transitionTo(self.S4_GET_DELTA)
                    
                    elif (user_input.decode('utf-8')  == ('h' or 'H')): # Check input
                        self.transitionTo(self.S5_PRINT_COMMANDS)    
                    else:
                        print('Invalid Command. Press \'h\' for help. \n')
                        pass
                    
            elif(self.state == self.S2_SET_POSITION):
                # Run State 2 Code
                shares.cmd = True      # Wait for response from TaskEncoder
                print('The encoder has been zero-ed \n')
                self.transitionTo(self.S1_WAIT_CMD)
    
            elif(self.state == self.S3_GET_POSITION):
                # Run State 3 Code
                print('The encoder position is: ' + str(shares.position) + '. \n')
                self.transitionTo(self.S1_WAIT_CMD)
                
            elif(self.state == self.S4_GET_DELTA):
                # Run State 4 Code
                print('The encoder delta is: ' + str(shares.delta) + '. \n')
                self.transitionTo(self.S1_WAIT_CMD)
                
            elif(self.state == self.S5_PRINT_COMMANDS):
                # Run State 5 Code
                self.clear()
                self.print_commands()
                self.transitionTo(self.S1_WAIT_CMD)
                
            else:
                pass
            self.runs += 1 # Update run count
            self.next_time += self.interval # Update the "Scheduled" timestamp 


    def transitionTo(self, newState):
        '''
        @brief Updates the state variable.
        '''
        self.state = newState