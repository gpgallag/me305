'''
@file Shares.py

@brief A container for all the inter-task variables.

@author Charlie Refvem

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/

@date N/A
'''

## The shared command character sent from TaskUser to TaskEncoder.
cmd = None
## The shared encoder position variable used betwen TaskEncoder to TaskUser.
position = None
## The shared encoder delta variable used betwen TaskEncoder to TaskUser.
delta = None


