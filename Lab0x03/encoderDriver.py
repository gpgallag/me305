'''
@file encoderDriver.py

@brief This file contains a driver for a Hall-effect encoder in quadrature.

@details The driver is initialized with the requisite timer, timer channels, and pin
locations for the Nucleo L476RG microprocessor, as inputted by the user.
The driver does *not* check whether the desired parameters are acceptable - please
refer to the L476RG data sheet for more information on hardware. The driver
also corrects for under/overflow.

@package Lab0x03

@author Grant Gallagher

@date November 17, 2020
'''
import pyb

class EncoderDriver():
    '''
    @brief      Encapsulates the operation of a timer to read from an encoder.
    
    @details    This class initializes the pin locations, timer, and timer
                channels (for each pin) for an encoder. The class reads the
                position of the encoder from two Hall effect sensors in quadrature
                while correcting for under/overflow. A run task is created to
                constantly update the encoder position and delta at a regular
                interval to avoid missing encoder ticks.
    '''
    def __init__(self, pin_A, pin_B, timer, debug):
        '''
        @brief       Constructs an EncoderDriver object.
        @param pin_A (1/2) of the signal pins for the hall-effect sensor on the encoder.
        @param pin_B (2/2) of the signal pins for the hall-effect sensor on the encoder.
        @param timer A timer object with prescalar and period used to construct the timer channels.
        @param debug A boolean value that determines debugging state.
        '''
        ## Pin attribute for (1/2) Hall sensors on encoder.
        self.pin_A = pin_A
        
        ## Pin attribute for (2/2) Hall sensors on encoder.
        self.pin_B = pin_B
        
        ## Instance of timer attribute.
        self.timer = timer
        
        ## Timer channel attribute initialized to pin_A.
        self.timer_ch1 = self.timer.channel(1,pyb.Timer.ENC_A, pin=self.pin_A)
        
        ## Timer channel attribute initialized to pin_B.
        self.timer_ch2 = self.timer.channel(2,pyb.Timer.ENC_B, pin=self.pin_B)
        
        ## Second most recent recorded encoder position.
        self.position_old = 0
        
        ## Most recent recorded encoder position.
        self.position_new = 0
        
        self.speed = 0
        
        ## The count difference between position_new and position_old.
        self.delta = 0
        
        ## The debug state (bool).
        self.debug = debug
        
        if self.debug: # When debug == true
            print('Creating an Encoder Driver Object')
        
        
    def update(self):
        '''
        @brief Corrects under/overflow for the encounter count and advances
               delta, most recent position, and second most recent position.
        '''
        # print("Count: " + str(self.timer.counter()))
        # print("Pos. : " + str(self.position_new))
        self.delta = self.timer.counter() - (self.position_new % 0xFFFF) # Difference between last and current position
        # print("Delta: " + str(self.delta))
    
        if self.delta > 0xFFFF/2: # Check for underflow
            self.delta -= (0xFFFF + 1)                    # Fixes underflow (for 16-bit encoder)
            
        elif self.delta < -0xFFFF/2: # Check for overflow
            self.delta += (0xFFFF + 1)                    # Fixes overflow (for 16-bit encoder)
            
        else:
            pass
        # print("Dcorr: " + str(self.delta))
        self.position_old = self.position_new               # Advance second most recent encoder position
        self.position_new  = self.position_old + self.delta # Advance most recent coder position
        if self.debug: # When debug == true
            print('Encoder Position: ' + str(self.position_new))
            print('Encoder Delta: ' + str(self.delta))
    
    def get_position(self):
        '''
        @brief  Returns the absolute value of the encoder position since initialization
                or zero-ing -- accounting for over/underflow.
        @return The most recent value of the encoder position.
        '''
        return self.position_new
    
    def get_old_position(self):
        return self.position_old
        
    
    def set_position(self, setValue):  
        '''
        @brief          Sets all forms of the current encoder position to a specified value.
        @param setValue An integer value for the encoder position (count) to be set to.
                        By default, setValue is 0 in order to zero the encoder.
        ''' 
        self.position_old = setValue 
        self.position_new = setValue
        self.timer.counter(setValue)
    
    
    def get_delta(self):
        '''
        @brief      Updates and returns the difference in encoder position.
        @return     The difference between position_new and position_old.
        '''
        return self.delta