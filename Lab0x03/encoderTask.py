'''
@file encoderTask.py

@brief This file contains a finite-state machine to run an encoderDriver object.

@details The class EncoderTask interacts between an EncoderDriver and UserTask object
using shared variabled: cmd and resp. The FSM has five possible states: (1) initialization, 
(2) update the encoder position/delta, (3) set the encoder position to 0, (4) get the
encoder position for the shared response to UserTask, (5) get the encoder delta for the shared
response to UserTask.

@author Grant Gallagher

@date October 20, 2020
'''
import shares
import pyb
import encoderDriver

class EncoderTask():
    '''
    @brief      A finite-state machine used to control an EncoderDriver object.
    
    @details    This class continuously updates the current state of a Hall-effect
                encoder using object methods in encoderDriver. This class also serves
                as a communication bridge between the encoder and UI. It accepts
                various commands from shared varriables, interacts with the encoder
                and respondes to the UI via shared variables.
    '''
    ## State 0: Initialization.
    S0_INIT         = 0
    
    ## State 1: Update the encoder position/delta.
    S1_UPDATE       = 1
    
    ## State 2: Set the encoder position.
    S2_SET_POSITION = 2
    
    # ## State 3: Get the encoder position.
    # S3_GET_POSITION = 3
    
    # ## State 3: Get the encoder delta.
    # S4_GET_DELTA    = 4

    def __init__(self, interval, encoder_object):
        '''
        @brief Constructs an EncoderTask object.
        @param interval An integer number of microseconds between desired runs of the task.
        @param encoder_object  The EncoderDriver object which parameters are being read.
        '''
        ## Instance of an EncoderDriver object.
        self.encoder_object = encoder_object
        
        ##  The amount of time in milliseconds between runs of the task.
        self.interval = interval
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run.
        self.runs = 0
        
        ## The timestamp for the first iteration.
        self.start = pyb.millis()
        
        ## The "timestamp" for when the task should run next.
        self.next_time = self.interval + pyb.elapsed_millis(self.start)


    def run(self):
        '''
        Runs one iteration of the task.
        '''
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if (pyb.elapsed_millis(self.start) >= self.next_time):
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.encoder_object.set_position(0)   # Zero the encoder
                self.transitionTo(self.S1_UPDATE)
            
            elif(self.state == self.S1_UPDATE):
                # Run State 1 Code
                self.encoder_object.update()
                shares.position = self.encoder_object.get_position()
                shares.delta = self.encoder_object.get_delta()
                # print('Elapsed Time: ' + str(round(float(pyb.elapsed_millis(self.start)/1000),1)) + 's | Position: ' + str(self.encoder.get_position()))
                if shares.cmd:
                    self.transitionTo(self.S2_SET_POSITION)
                    
            elif(self.state == self.S2_SET_POSITION):
                # Run State 2 Code
                self.encoder_object.set_position(0)                # Zero the encoder
                self.transitionTo(self.S1_UPDATE)
                # shares.resp = ('The encoder has been zero-ed. \n') # Update shared resp variable
                shares.cmd = None                                    # Reset the shared cmd variable

            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1 # Update run count
            self.next_time += self.interval # Update the "Scheduled" timestamp   


    def transitionTo(self, newState):
        '''
        Updates the state variable.
        '''
        self.state = newState