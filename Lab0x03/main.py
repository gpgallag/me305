'''
@file main.py

@brief The main file that constructs the EncoderDriver, EncoderTask, and UserTask
objects to be run continuously on the Nucleo L476RG.

@author Grant Gallagher

@date October 20, 2020
'''
from encoderDriver import EncoderDriver
from encoderTask import EncoderTask
from userTask import UserTask
import pyb

Apin = pyb.Pin(pyb.Pin.cpu.A6)                 # Construct the encoder pin object (1/2)
Bpin = pyb.Pin(pyb.Pin.cpu.A7)                 # Construct the encoder pin object (2/2)
tim = pyb.Timer(3, prescaler=0, period=0xFFFF) # Construct the timer object

encoder_1 = EncoderDriver(Apin,Bpin,tim,debug=False) # Construct an EncoderDriver object
task_1 = EncoderTask(10, encoder_1)      # Construct an EncoderTask object for encoder1 with 10ms interval
task_2 = UserTask(10)                   # Construct a UserTask object with 10ms interval

TaskList = [task_1, task_2]
while True:
    for task in TaskList:
        task.run()