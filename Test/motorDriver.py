'''
@file  motorDriver.py

@brief  This file contains a motor driver class.

@details  This file contains a single motor driver class that is used to drive a motor
          from the Nucleo L476RG board. The driver implements bidirectional, PWM control
          of at a user-specified frequency. The motor can be set to one of four various states
          found in the truth table of the DRV8847 chip: Sleep (disabled), Coast (enabled && duty = 0),
          Reverse Direction (duty < 0), or Forward Direction (duty > 0). Note: This class HAS
          BEEN ALTERED FROM THE ME305 SETUP DUE TO HARDWARE ISSUES. This class controls
          the motor with PH/EN control instead of direct PWM.

@package Lab0x06
@package Lab0x07

@author Grant Gallagher

@date December 4, 2020
'''
import pyb

class MotorDriver:
    '''
    @brief   This class implements a motor driver for the ME405 board.
    
    @details This motor driver class implements bidirectional, PH/EN control of
             a DCX-22S Brushed DC motor which would be found on the ME405 breakout board.
             The motor can be enabled, disabled, or set to a PWM duty cycle between
             -100 and 100 (for each direction).
    '''
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer, debug):
        '''
        @brief Creates a motor driver object
        @details Initializes the GPIO pins onboard the Nucleo, sets up the timer channel,
                 and disables the motor by default for safety.
        @param nSLEEP_pin  A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer   A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        @param debug A boolean value that determines debugging state.
        @code Example:
            # Initialize pins B10 and A8 to drive the motor, and initialize timer channel 2 to 20kHz.
            pin_nSleep = pyb.Pin(pyb.Pin.cpu.A9)
            pin_IN1 = pyb.Pin(pyb.Pin.cpu.B10)
            pin_IN2 = pyb.Pin(pyb.Pin.cpu.A8)
            tim2 = pyb.Timer(2, freq = 20000)
            
            # Construct MotorDriver object
            moe = MotorDriver(pin_nSleep, pin_IN1, pin_IN2, tim2, debug = False)
        '''
        ## The pin object for the sleep mode on the DRV8847 chip from the Nucleo.
        self.nSLEEP_pin = nSLEEP_pin
        
        ## The pin object for the negative motor output from the Nucleo.
        self.IN1_pin = IN1_pin
        
        ## The pin object for the positive motor output from the Nucleo.
        self.IN2_pin = IN2_pin
        
        ## The timer object of which the timer channels will run one.
        self.timer = timer
        
        nSLEEP_pin.init(pyb.Pin.OUT_PP) # Configure the pin for output, with push-pull control
        IN1_pin.init(pyb.Pin.OUT_PP)    # Configure the pin for output, with push-pull control
        IN2_pin.init(pyb.Pin.OUT_PP)    # Configure the pin for output, with push-pull control
        nSLEEP_pin.low()                # Initialize the motor to "sleep" mode to avoid damage.
        
        ## The timer channel used to control IN1_pin with PWM.
        self.timer_ch3 = self.timer.channel(3, pyb.Timer.PWM, pin=self.IN1_pin)
        
        ## The debug state (bool).
        self.debug = debug
        
        if self.debug:
            print('Creating a Motor Driver Object')
                    
            
    def enable(self):
        '''
        @brief  Enables the motor.
        @details Raises the nSleep pin so that the motor can be driven.
        '''
        self.nSLEEP_pin.high() # Raise the sleep mode pin
        
        if self.debug: # When debug == true
            print('Enabling Motor')
    
    
    def disable(self):
        '''
        @brief  Disables the motor and sets the PWM duty cycle to 0.
        @details Lowers the nSleep so that the motor cannot be driven.
        '''
        self.nSLEEP_pin.low() # Lower the sleep mode pin
        self.set_duty(0)      # Turn off signals for redundancy
        
        if self.debug:
            print('Disabling Motor')
            
    
    def set_duty(self, duty):       
        '''
        @brief Sets the duty cycle to be sent to the motor.
        @details Positive values cause effort in one direction, negative values in the opposite direction.
        @param duty  A signed integer between -100 and 100, holding the duty cycle of the PWM signal sent to the motor
        @code Example:
            
            motorDriverObject.enable()
            motorDriverObject.set_duty(50) # Drives the motor forward at 50% power
            
            motorDriverObject.set_duty(-70) # Drives the motor in reverse at 70% power
            
            motorDriverObject.set_duty(0) # Stops the movemement of the motor
            
            motorDriverObject.set_duty(200) # Has no effect on the motor - out of range
            
            motorDriverObject.set_duty(81.43) # Has no effect on the motor - not an integer
        '''
        if (type(duty) != int) or (abs(duty) > 100):
            if self.debug: # When debug == true
                print('Error. duty must be an integer between -100 and 100')
            pass
        
        elif duty == 0: # Motor Coast
            self.IN1_pin.low()                    # Lower IN1_pin
            self.IN2_pin.low()                    # Lower IN1_pin
            self.timer_ch3.pulse_width_percent(0) # Set duty cycle to 0
            
        elif duty > 0: # Motor Forward
            self.IN1_pin.high()                       # Lower IN1_pin
            self.IN2_pin.high()                      # Raise IN2_pin
            self.timer_ch3.pulse_width_percent(duty)    # Set duty cycle to 0
        
        elif duty < 0: # Motor Reverse 
            self.IN1_pin.high()                           # Raise IN1_pin
            self.IN2_pin.low()                            # Lower IN2_pin
            self.timer_ch3.pulse_width_percent(abs(duty)) # Set duty cycle to absolute value of input argument
            
        if self.debug: # When debug == true
            print('Duty cycle set to: ' + str(duty))
     
        
'''
if __name__ == '__main__':
    # The following code is a test program for the motor class. Any code within the
    # if __name__ == '__main__' block will only run when the script is executed as
    # a standalone program. If the script is imported as a module the code block will not run.
    ## The sleep mode pin object initialized to A15
    pin_nSleep = pyb.Pin(pyb.Pin.cpu.A9)
    
    ## The output pin object initialized to B10
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B10)
    
    ## The output pin object initialized to A8
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.A8)
   
    ## The timer object used for PWM generation
    tim = pyb.Timer(2, freq = 20000)
    
    ## The motor object passing in the pins and timer
    moe = MotorDriver(pin_nSleep, pin_IN1, pin_IN2, tim, debug = True)
    
    # Enable the motor driver
    moe.enable()
    print("Setting up lab 6")
    while True:
        ## The user input to control the PWM duty cycle value.
        val = int(input("Set the PWM: ")) # Prompt user input
        moe.set_duty(val)                 # Set the duty cycle to user input
'''