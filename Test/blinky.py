'''
@file main.py

@author Grant Gallagher

@copyright Copyright 2020, Grant Gallagher, All rights reserved.

@date October 13, 2020

This files defines the physicalLED and virtualLED objects and runs the output of a virtual/physical LED.
'''

from blinky import virtualLED, physicalLED

## The pin header that is assigned to the physical LED on the Nucleo
pinA5 = pyb.Pin(pyb.Pin.cpu.A5)

## A Timer object using timer 2 on the Nucleo
tim2  = pyb.Timer(2, freq = 20000)

## A Channel object created on tim2 , set up to output PWM on pina5
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

if __name__ == '__main__':
    ## A virtualLED object set to blink with a period of 2 seconds.
    task2_Virtual = virtualLED(2000)
    
    ## A physicalLED object set to a sinusoidal shape with a period of 10 seconds - alternating between sawtooth every 30 seconds.
    task1_Physical = physicalLED(t2ch1, 10000, 'SIN', 30000)
    
    while True: # Runs in a constant loop for hardware.
        task1_Physical.run()
        task2_Virtual.run()