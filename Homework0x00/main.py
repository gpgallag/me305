'''
@file main.py

@author Grant Gallagher

@copyright Copyright 2020, Grant Gallagher, All rights reserved.

@date October 7, 2020

This files defines elevator objects of Button and MotorDriver, used to call TaskElevator.
'''

from elevator import Button, MotorDriver, TaskElevator

## Motor Object
motor = MotorDriver()

## Sensor Object for the "first floor".
first = Button('PB6')

## Sutton Object for the "second floor".
second = Button('PB7')

## Button Object for the "first floor".
button_1 = Button('PB8')

## Button Object for the "second floor".
button_2 = Button('PB9')

## Task Object that is to run the TaskElevator method.
task1 = TaskElevator(0.1, motor, button_1, button_2, first, second, 1) # Will also run constructor

## Task Object that is to run the TaskElevator method.
task2 = TaskElevator(0.5, motor, button_1, button_2, first, second, 2) # Will also run constructor


# To run task1.run() and task2.run() for a small duration.
# The two tasks should run in parallel with one another.
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run() # TaskElevator Object  #1
    task2.run() # TaskElevator Object #2