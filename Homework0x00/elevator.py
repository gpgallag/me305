'''
@file elevator.py

This file serves as an implementation of a finite-state-machine using
Python. The progam will implement some code to control an imaginary two-story
elevator.

The user has two buttons, button_1 and button_2, used to direct the elevator to the first or second floor.
There are two proximity buttons, first and second, on floors one and two that signal the elevator motor to stop.

@package Hw0x01
@brief This package contains elevator.py.

@details This package is used to run a finite-state machine that controls an imaginary elevator.

@author Grant Gallagher

@date October 6, 2020
'''


from random import choice
import time
     
class TaskElevator:
    '''
    @brief      A finite state machine to control the motor and position of an elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0: Initialization
    S0_INIT           = 0

    ## Constant defining State 1: The elevator is moving down, from the second to first floor.
    S1_MOVING_DOWN    = 1
    
    ## Constant defining State 2: The elevator is moving up, from the first floor to the second floor.
    S2_MOVING_UP      = 2
    
    ## Constant defining State 3: The elevator is stopped on the first floor.
    S3_STOPPED_FIRST  = 3
    
    ## Constant defining State 4: The elevator is stopped on the second floor.
    S4_STOPPED_SECOND = 4


    def __init__(self, interval, motor, button_1, button_2, first, second, elevator_num):
        '''
        @brief      Creates a TaskElevator object.
        @param interval     The number of seconds to run the task at.
        @param motor        The current state of the motor (0 = off / 1 = up / 2 = down).
        @param button_1     The button to send the motor to the first floor.
        @param button_2     The button to send the motor to the second floor.
        @param first        The proximinity sensor for the first floor.
        @param second       The proximity sensor for the second floor.
        @param elevator_num The identification number for the elevator.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the Motor object.
        self.motor = motor
        
        ## The Button object used for the first floor button.
        self.first = first
        
        ## The Button object used for the second floor button.
        self.second = second
        
        ## The Button object used for the first floor button.
        self.button_1 = button_1
        
        ## The Button object used for the second floor button.
        self.button_2 = button_2
        
        ## The identification number for the elevator.
        self.elevator_num = elevator_num
        
        ## A counter showing the number of times the task has run.
        self.runs = 0
        
        ## The timestamp for the first iteration.
        self.start_time = time.time()
    
        ## The interval of time, in seconds, between runs of the task.
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next.
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task.
        @details    The task can be one of five various states: initialization, moving down,
                    moving up, stopped on the first floor, or stopped on the second floor.
        '''
        ## The "timestamp" for the current task.
        self.curr_time = time.time()    # Updating the current timestamp
        
        # Checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            
            
            if(self.state == self.S0_INIT):
                # Display current: run iteration count, State number, time since initialization
                print('Run ' + str(self.runs) + ' | Elevator ' + str(self.elevator_num) + ' | STATE 0 | elapsed time: {:0.2f}'.format(self.curr_time - self.start_time) + 's')
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN) # State    = 3
                self.motor.down()                      # motor    = 2
                self.button_1.clearButton()            # button_1 = 0
                self.button_2.clearButton()            # button_2 = 0
                self.first.clearButton()               # first    = 0
                self.second.clearButton()              # second   = 0
                
            elif(self.state == self.S1_MOVING_DOWN):
                # Display current: run iteration count, State number, time since initialization
                print('Run ' + str(self.runs) + ' | Elevator ' + str(self.elevator_num) + ' | STATE 1 | motor = ' + str(self.motor.motorValue) + ' | elapsed time: {:0.2f}'.format(self.curr_time - self.start_time) + 's')
                # Run State 1 Code
                if self.first.getButtonState() == 1:
                    self.transitionTo(self.S3_STOPPED_FIRST)    # State    = 3
                    self.button_1.clearButton()                 # button_1 = 0
                    self.motor.stop()                           # motor    = 0
            
            elif(self.state == self.S2_MOVING_UP):
                # Display current: run iteration count, State number, time since initialization
                print('Run ' + str(self.runs) + ' | Elevator ' + str(self.elevator_num) + ' | STATE 2 | motor = ' + str(self.motor.motorValue) + ' | elapsed time: {:0.2f}'.format(self.curr_time - self.start_time) + 's')
                # Run State 2 Code
                if self.second.getButtonState() == 1: 
                    self.transitionTo(self.S4_STOPPED_SECOND) # State    = 4
                    self.button_2.clearButton()               # button_2 = 0
                    self.motor.stop()                         # motor    = 0
            
            elif(self.state == self.S3_STOPPED_FIRST):
                # Display current: run iteration count, State number, time since initialization
                print('Run ' + str(self.runs) + ' | Elevator ' + str(self.elevator_num) + ' | STATE 3 | motor = ' + str(self.motor.motorValue) + ' | elapsed time: {:0.2f}'.format(self.curr_time - self.start_time) + 's')
                # Run State 3 Code
                if self.button_2.getButtonState() == 1:
                    self.transitionTo(self.S2_MOVING_UP)    # State    = 2
                    self.first.clearButton()                # first    = 0
                    self.motor.up()                         # motor    = 1
            
            elif(self.state == self.S4_STOPPED_SECOND):
                # Display current: run iteration count, State number, time since initialization
                print('Run ' + str(self.runs) + ' | Elevator ' + str(self.elevator_num) + ' | STATE 4 | motor = ' + str(self.motor.motorValue) + ' | elapsed time: {:0.2f}'.format(self.curr_time - self.start_time) + 's')
                # Run State 4 Code
                if self.button_1.getButtonState() == 1:
                    self.transitionTo(self.S1_MOVING_DOWN) # State    = 2
                    self.second.clearButton()              # second = 0
                    self.motor.down()                      # motor    = 2
            
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1                  # Increment the count of tasks performed
            self.next_time += self.interval # Update the "Scheduled" timestamp
            
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run.
        '''
        self.state = newState
        
class Button:
    '''
    @brief      An elevator Button class
    @details    This class represents buttons that the can be pushed by the
                imaginary user to direct the elevator to go to the first or second floor.
                As of right now this class is implemented using "pseudo-hardware".
                That is, we are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object.
        @param pin  A pin object that the button is connected to.
        '''
        
        ## The pin object used to read the Button object state.
        self.pin = pin
        
        ## The buttonValue object that is used to store the Button object value.
        self.buttonValue = 0
        
        print('Button object created and set to '+ str(self.pin))
    
    def getButtonState(self):
        '''
        @brief      Checks the button state by assigning a choice of 0 or 1.
        @details    This action replicates an imaginary user pressing the button,
                    or the floor sensor detecting the elevator.
                    Since there is no hardware attached this method
                    returns a randomized 1 or 0 value. A value of 1 means
                    that the button is active. A value of 0
                    means that the button in not active.
        @return     An integer value of 0 or 1 representing the state of the button.
        '''
        if self.buttonValue == 0:
            self.buttonValue = choice([0, 1])
        else:
            self.buttonValue = 0
            
        return self.buttonValue

    def clearButton(self):
        '''
        @brief      Sets the button state to 0.
        @details    The button state is set to a value of 0 which represents
                    an 'off' position (imagine the button illumination is turned off).
        '''      
        self.buttonValue = 0
        

    
class MotorDriver:
    '''
    @brief      An elevator MotorDriver class.
    @details    This class represents a motor driver used to lift the elevator
                up, down, and hold it in place.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        ## The motor object used to read the motor state
        self.motorValue = 0
        
        print('Motor object created and set to ' + str(self.motorValue))
        pass
    
    def up(self):
        '''
        @brief Moves the motor up.
        @brief The motor is set to an integer value of 1.
               The motor is on, and is moving the elevator up.
        '''
        self.motorValue = 1
        print()
        print('- - - - - - - - - - -  Motor is pulling up.  - - - - - - - - - -') 
    
    def down(self):
        '''
        @brief Moves the motor down.
        @brief The motor is set to an integer value of 2.
               The motor is on, and is moving the elevator down.
        '''
        self.motorValue = 2
        print()
        print('- - - - - - - - - - -  Motor is pulling down.  - - - - - - - - -')   
    
    def stop(self):
        '''
        @brief Turns the motor off.
        @brief The motor is set to an integer value of 0.
               The motor is off, and the elevator is stationary.
        '''
        self.motorValue = 0
        print()
        print('- - - - - - - - - - - Motor is stationary.  - - - - - - - - - -')
        
    def getMotorState(self):
        '''
        @brief      Gets the motor state.
        @details    This method returns the state of the motor in the form of an integer.
                    A value of 0 represents the motor is stationary.
                    A value of 1 represents the motor is moving the elevator up.
                    A value of 2 represents the motor is moving the elevator down.
        @return     An integer representing the state of the motor.
        '''   
        print("The motor state is " + str(self.motorValue))
        
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Test and debugging code 
# if __name__ == '__main__':  
#     motor_Test = MotorDriver()
#     first_Test = Button('PB6')
#     second_Test = Button('PB7')
#     button_1_Test = Button('PB8')
#     button_2_Test = Button('PB9')
#     task1_Test = TaskElevator(0.1, motor_Test, button_1_Test, button_2_Test, first_Test, second_Test, 1) # Will also run constructor
#     task2_Test = TaskElevator(0.5, motor_Test, button_1_Test, button_2_Test, first_Test, second_Test, 2) # Will also run constructor
#     for N in range(10000000): #Will change to   "while True:" once we're on hardware
#         task1_Test.run()
#         task2_Test.run()
